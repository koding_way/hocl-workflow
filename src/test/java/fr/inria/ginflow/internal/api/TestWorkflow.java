/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.internal.api;

import org.testng.Assert;
import org.testng.annotations.Test;

import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.internal.helper.WorkflowHelper;
import fr.inria.hocl.core.hocli.Solution;

/**
 * @author msimonin
 * 
 */
@Test(groups = { "main" })
public class TestWorkflow  {

    @Test()
    public void test_workflow_creation() {
        Workflow workflow = WorkflowHelper.workflow1(WorkflowHelper.T1(), WorkflowHelper.T2(), WorkflowHelper.T3());
        Assert.assertEquals(workflow.valueSize(), 3);
        Assert.assertTrue(workflow.getTerminalServices().contains("T3"));
        Assert.assertFalse(workflow.getTerminalServices().contains("T2"));
        Assert.assertFalse(workflow.getTerminalServices().contains("T1"));
    }

    @Test()
    public void test_workflow_getService() {
        GinflowTuple T1 = WorkflowHelper.T1();
        Workflow workflow = WorkflowHelper.workflow1(T1, WorkflowHelper.T2(), WorkflowHelper.T3());
        GinflowTuple service = workflow.getService("T1");
        Assert.assertEquals(service, T1);
    }

    @Test()
    public void test_workflow_getdiffService() {
        GinflowTuple T1 = WorkflowHelper.T1();
        // force the creation of a new service.
        Workflow workflow = WorkflowHelper.workflow1(WorkflowHelper.T1(), WorkflowHelper.T2(), WorkflowHelper.T3());
        GinflowTuple service = workflow.getService("T1");
        Assert.assertNotEquals(service, T1);
    }

    @Test()
    public void test_workflow_change_solution_of_a_service() {
        GinflowTuple T1 = WorkflowHelper.T1();
        Workflow workflow = WorkflowHelper.workflow1(WorkflowHelper.T1(), WorkflowHelper.T2(), WorkflowHelper.T3());        
        workflow.getService("T1").setValue(new Solution());
        Assert.assertEquals(workflow.getService("T1").valueSize(), 0);
    }

}
