/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.executor.api.impl;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.testng.annotations.DataProvider;

import fr.inria.ginflow.internal.api.Command;
import fr.inria.ginflow.internal.api.Destination;
import fr.inria.ginflow.internal.api.DestinationControl;
import fr.inria.ginflow.internal.api.GinflowTuple;
import fr.inria.ginflow.internal.api.In;
import fr.inria.ginflow.internal.api.RebranchingInformation;
import fr.inria.ginflow.internal.api.Service;
import fr.inria.ginflow.internal.api.Source;
import fr.inria.ginflow.internal.api.SourceControl;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.options.OptionException;
import fr.inria.ginflow.options.Options;

public class TestExecutor {
	/**
	 * 
	 * Construct a service in sequence.
	 * 
	 * Ti-1 -> Ti -> Ti+1
	 * 
	 * @param max
	 *            max number of service in the sequence
	 * @param i
	 *            current id in the sequence
	 * @param command
	 *            the command to run
	 * @return The built service.
	 */
	protected GinflowTuple Ti(int max, int i, String command) {
		Service service = Service.newService(String.valueOf(i)).setCommand(Command.newCommand(command));
		Destination destination = Destination.newDestination();
		Source source = Source.newSource();
		if (i < max - 1) {
			destination = Destination.newDestination().add(String.valueOf(i + 1));
		}
		if (i > 0) {
			source = Source.newSource().add(String.valueOf(i - 1));
		}

		return service.setSource(source).setDestination(destination);
	}

	protected GinflowTuple Tc(int i, int src, int dst, String command) {
		Service service = Service.newService(String.valueOf(i)).setCommand(Command.newCommand(command));
		Destination destination = Destination.newDestination();
		Source source = Source.newSource();
		destination = Destination.newDestination().add(String.valueOf(dst));
		source = Source.newSource().add(String.valueOf(src));

		return service.setSource(source).setDestination(destination);
	}

	/**
	 * 
	 * Construct a service in sequence (control only).
	 * 
	 * @param max
	 *            max number of service in the sequence
	 * @param i
	 *            current id in the sequence
	 * @param command
	 *            the command to run
	 * @return The built service.
	 */
	protected GinflowTuple TiControl(int max, int i, String command) {
		Service service = Service.newService(String.valueOf(i)).setCommand(Command.newCommand(command));
		DestinationControl destination = DestinationControl.newDestinationControl();
		SourceControl source = SourceControl.newSourceControl();

		if (i < max - 1) {
			destination = DestinationControl.newDestinationControl().add(String.valueOf(i + 1));
		}
		if (i > 0) {
			source = SourceControl.newSourceControl().add(String.valueOf(i - 1));
		}

		return service.setSourceControl(source).setDestinationControl(destination);
	}

	protected GinflowTuple TcControl(int i, int src, int dst, String command) {
		Service service = Service.newService(String.valueOf(i)).setCommand(Command.newCommand(command));
		DestinationControl destination = DestinationControl.newDestinationControl();
		SourceControl source = SourceControl.newSourceControl();

		destination = DestinationControl.newDestinationControl().add(String.valueOf(dst));

		source = SourceControl.newSourceControl().add(String.valueOf(src));

		return service.setSourceControl(source).setDestinationControl(destination);
	}

	/**
	 * 
	 * Build a sequence of max services, each running the same command.
	 * 
	 * @param max
	 *            The number of services
	 * @param command
	 *            The command
	 * @return
	 */
	protected Workflow sequence(int max, String command) {
		Workflow workflow = Workflow.newWorkflow("sequence");
		for (int i = 0; i < max; i++) {
			workflow.registerService(String.valueOf(i), Ti(max, i, command));
		}
		// set the last service has terminal.
//		workflow.getTerminalServices().add(String.valueOf(max - 1));
		return workflow;
	}

	/**
	 * 
	 * Build a sequence of max services with control dependency only, each
	 * running the same command.
	 * 
	 * @param max
	 *            The number of services
	 * @param command
	 *            The command
	 * @return
	 */
	protected Workflow sequenceControl(int max, String command) {
		Workflow workflow = Workflow.newWorkflow("sequence");
		for (int i = 0; i < max; i++) {
			System.out.println("REGISTRATION");
			workflow.registerService(String.valueOf(i), TiControl(max, i, command));
		}
//		// set the last service has terminal.
//		workflow.getTerminalServices().add(String.valueOf(max - 1));
		return workflow;
	}

	/**
	 * 
	 * Build a merge workflow.
	 * 
	 * @param max
	 *            Number of service to merge
	 * @return
	 */
	protected Workflow merge(int max) {
		Workflow workflow = Workflow.newWorkflow("Wmerge");
		Source source = Source.newSource();
		String sName;
		for (int i = 0; i < max; i++) {
			sName = String.valueOf(i);
			GinflowTuple service = Service.newService(sName)
					.setDestination(Destination.newDestination().add(String.valueOf(max)))
					.setCommand(Command.newCommand("echo")).setIn(In.newIn().add(String.valueOf(i)));
			workflow.registerService(sName, service);
			source.add(sName);
		}
		// add the final service
		sName = String.valueOf(max);
		workflow.registerService(sName, Service.newService(sName).setCommand(Command.newCommand("echo"))
				.setSource(source).setIn(In.newIn().add(String.valueOf(max))));

		// set the last service has terminal.
//		workflow.getTerminalServices().add(String.valueOf(max));

		return workflow;
	}

	/**
	 * 
	 * Build a merge workflow (control only).
	 * 
	 * @param max
	 *            Number of service to merge
	 * @return
	 */
	protected Workflow mergeControl(int max) {
		Workflow workflow = Workflow.newWorkflow("Wmerge");
		SourceControl source = SourceControl.newSourceControl();
		String sName;
		for (int i = 0; i < max; i++) {
			sName = String.valueOf(i);
			GinflowTuple service = Service.newService(sName)
					.setDestinationControl(DestinationControl.newDestinationControl().add(String.valueOf(max)))
					.setCommand(Command.newCommand("echo")).setIn(In.newIn().add(String.valueOf(i)));
			workflow.registerService(sName, service);
			source.add(sName);
		}
		// add the final service
		sName = String.valueOf(max);
		workflow.registerService(sName, Service.newService(sName).setCommand(Command.newCommand("echo"))
				.setSourceControl(source).setIn(In.newIn().add(String.valueOf(max))));

		// set the last service has terminal.
//		workflow.getTerminalServices().add(String.valueOf(max));

		return workflow;
	}

	/**
	 * 
	 * Build a split workflow.
	 * 
	 * @param max
	 *            Number of service in the split (total = max + 1)
	 * @return
	 */
	protected static Workflow split(int max) {
		Workflow workflow = Workflow.newWorkflow("Wmerge");

		Destination destination = Destination.newDestination();
		String sName;
		for (int i = 1; i <= max; i++) {
			sName = String.valueOf(i);
			GinflowTuple service = Service.newService(sName).setSource(Source.newSource().add("0"))
					.setCommand(Command.newCommand("echo")).setIn(In.newIn().add(String.valueOf(i)));
			workflow.registerService(sName, service);
			destination.add(sName);
//			workflow.getTerminalServices().add(String.valueOf(i));
		}
		// add the final service
		sName = String.valueOf(0);

		workflow.registerService(sName, Service.newService(sName).setCommand(Command.newCommand("echo"))
				.setDestination(destination).setIn(In.newIn().add("!")));

		return workflow;
	}

	/**
	 * 
	 * Build a split workflow (control only).
	 * 
	 * @param max
	 *            Number of service in the split (total = max + 1)
	 * @return
	 */
	protected static Workflow splitControl(int max) {
		Workflow workflow = Workflow.newWorkflow("Wmerge");

		DestinationControl destination = DestinationControl.newDestinationControl();
		String sName;
		for (int i = 1; i <= max; i++) {
			sName = String.valueOf(i);
			GinflowTuple service = Service.newService(sName).setSourceControl(SourceControl.newSourceControl().add("0"))
					.setCommand(Command.newCommand("echo")).setIn(In.newIn().add(String.valueOf(i)));
			workflow.registerService(sName, service);
			destination.add(sName);
//			workflow.getTerminalServices().add(String.valueOf(i));
		}
		// add the final service
		sName = String.valueOf(0);

		workflow.registerService(sName, Service.newService(sName).setCommand(Command.newCommand("echo"))
				.setDestinationControl(destination).setIn(In.newIn().add("!")));

		return workflow;
	}

	/**
	 * 
	 * Build a merge workflow (mixed control/data).
	 * 
	 * @param max
	 *            Number of service to merge
	 * @return
	 */
	protected Workflow mergeMixedDataControl(int max) {
		Workflow workflow = Workflow.newWorkflow("Wmerge");
		SourceControl sourceControl = SourceControl.newSourceControl();
		Source source = Source.newSource();
		String sName;
		for (int i = 0; i < max; i++) {
			sName = String.valueOf(i);
			Service service = Service.newService(sName).setCommand(Command.newCommand("echo"))
					.setIn(In.newIn().add(String.valueOf(i)));
			if (i % 2 == 0) {
				service.setDestinationControl(DestinationControl.newDestinationControl().add(String.valueOf(max)));
				sourceControl.add(sName);
			} else {
				service.setDestination(Destination.newDestination().add(String.valueOf(max)));
				source.add(sName);
			}
			workflow.registerService(sName, service);

		}
		// add the final service
		sName = String.valueOf(max);
		workflow.registerService(sName, Service.newService(sName).setCommand(Command.newCommand("echo"))
				.setSourceControl(sourceControl).setSource(source).setIn(In.newIn().add(String.valueOf(max))));

		// set the last service has terminal.
//		workflow.getTerminalServices().add(String.valueOf(max));

		return workflow;
	}

	/**
	 * 
	 * Build a sequence of 3 services 0 -> 1* -> 2 2 will be replaced by 3
	 * 
	 * @return
	 */
	protected Workflow sequence3WithAdaptiveness() {
		int max = 3;
		Workflow workflow = sequence(max, "echo !");
		RebranchingInformation rebranchingInformation = new RebranchingInformation();
		// 2 is supervised
		rebranchingInformation.addSupervised("1").addUpdateDst("2", "3").addUpdateSrc("0", "3");
		workflow.addRebranchingInformation(rebranchingInformation);
		// 3 is the alternative
		GinflowTuple s4 = Tc(3, 0, 2, "echo alternative!");
		workflow.registerService("3", s4);

		// we introduce the failure
		workflow.getService("1").setCommand(Command.newCommand("bla125"));
		return workflow;
	}

	/**
	 * 
	 * Build a sequence of 3 services 0 -> 1* -> 2 2 will be replaced by 3
	 * 
	 * @return
	 */
	protected Workflow sequenceControl3WithAdaptiveness() {
		int max = 3;
		Workflow workflow = sequenceControl(max, "echo !");
		RebranchingInformation rebranchingInformation = new RebranchingInformation();
		rebranchingInformation.addSupervised("1").addUpdateDstControl("2", "3").addUpdateSrcControl("0", "3");
		workflow.addRebranchingInformation(rebranchingInformation);

		// 3 is the alternative
		GinflowTuple s4 = TcControl(3, 0, 2, "echo alternative!");
		workflow.registerService("3", s4);

		// we introduce the failure
		workflow.getService("1").setCommand(Command.newCommand("bla126"));
		System.out.println(workflow);
		return workflow;
	}

	/**
	 * 
	 * Build a sequence of 3 services 0 -> 1* -> 2 2 will be replaced by several
	 * services in parallel : 0 -> 3|4... -> 2
	 * 
	 * @return
	 */
	protected Workflow sequence3bisWithAdaptiveness(int n) {
		int max = 3;
		Workflow workflow = sequence(max, "echo !");
		RebranchingInformation rebranchingInformation = new RebranchingInformation();

		// alternatives
		List<String> rebranches = new ArrayList<String>();
		for (int i = 3; i < 3 + n; i++) {
			GinflowTuple s4 = Tc(i, 0, 2, "echo " + i);
			workflow.registerService(String.valueOf(i), s4);
			rebranches.add(String.valueOf(i));
		}
		rebranchingInformation.addSupervised("1").addAllUpdateDst("2", rebranches).addAllUpdateSrc("0", rebranches);
		workflow.addRebranchingInformation(rebranchingInformation);

		// we introduce the failure
		workflow.getService("1").setCommand(Command.newCommand("bla 127"));
		return workflow;
	}

	/**
	 * 
	 * Build a sequence of 3 services 0 -> 1* -> 2 2 will be replaced by 3
	 * 
	 * @return
	 */
	protected Workflow sequenceControl6With2Adaptiveness() {
		int max = 6;
		Workflow workflow = sequenceControl(max, "echo !");

		RebranchingInformation rebranchingInformation = RebranchingInformation.newRebranchingInformation()
				.addSupervised("1").addUpdateDstControl("2", "6").addUpdateSrcControl("0", "6");
		workflow.addRebranchingInformation(rebranchingInformation);

		rebranchingInformation = RebranchingInformation.newRebranchingInformation().addSupervised("4")
				.addUpdateDstControl("5", "7").addUpdateSrcControl("3", "7");
		workflow.addRebranchingInformation(rebranchingInformation);

		// 6 is the alternative
		GinflowTuple s6 = TcControl(6, 0, 2, "echo alternative 6!");
		workflow.registerService("6", s6);

		// 7 is also alternative
		GinflowTuple s7 = TcControl(7, 3, 5, "echo alternative 7!");
		workflow.registerService("7", s7);

		// we introduce the failures
		workflow.getService("1").setCommand(Command.newCommand("bla127"));
		workflow.getService("4").setCommand(Command.newCommand("bla127"));
		return workflow;
	}

	/**
     * 
     * 1 --> 2 --> 3 replaced by 1 --> 4 --> 3
     * 
     * @return
     */
    protected Workflow updateWorkflow() {
    	Workflow workflow = Workflow.newWorkflow("updateWorkflow");
    	  
		workflow.registerService("4", Service.newService("4")
			  .setCommand(Command.newCommand("echo"))
			  .setSourceControl(SourceControl.newSourceControl().add("1"))
			  .setDestinationControl(DestinationControl.newDestinationControl().add("3"))
			  .setIn(In.newIn().add("4")));
	  
		RebranchingInformation rebranchingInformation = RebranchingInformation.newRebranchingInformation()
			.addSupervised("2")
			.addUpdateDstControl("3", "4")
			.addUpdateSrcControl("1", "4");
		
        workflow.addRebranchingInformation(rebranchingInformation);
        
        return workflow;
	        
    }
    
    protected Workflow[] sequenceControlWithUpdate(){
        Workflow workflow = this.sequenceControl(4, "echo");
        Service service = workflow.getService("2");
        service.setCommand(fr.inria.ginflow.internal.api.Command.newCommand("bla"));
        
        Workflow update =  updateWorkflow();
        
        return new Workflow[]{workflow, update};
    }
    

    /**
     * 
     * Provides a set of sample to test.
     * Intends to be use with a data provider.
     * 
     * @return
     */
    protected Object[][] provideExactTerminals() {
		Object[][] tests = {
				{
					sequenceControl3WithAdaptiveness(),
					// out, err, exit this will test with string.contains
					new String[]{"<\"!\">", "<\"\">", "<0>"}
				},
				{
					sequence(2, "echo !"),
					// out, err, exit this will test with string.contains
					new String[]{"<\"! !\">", "<\"\">", "<0>"},
				}, 
				{
					sequenceControl(2, "echo !"),
					// out, err, exit this will test with string.contains
					new String[]{"<\"!\">", "<\"\">", "<0>"}
				},
				{
					sequence3WithAdaptiveness(),
					// out, err, exit this will test with string.contains
					new String[]{"<\"! alternative! !\">", "<\"\">", "<0>"}
				},
				
		};
		return tests;
	}

	protected Object[][] provideFuzzyTerminals() {
		Object[][] tests = {
				{
					merge(3),
					// contains out
					new String[][]{
						{"3", "3", "<\"\">", "<0>"},
						{"3", "2", "<\"\">", "<0>"},
						{"3", "1", "<\"\">", "<0>"},
						{"3", "0", "<\"\">", "<0>"},
					}, 
					new String[][]{}
				},
				{
					mergeControl(3),
					// contains out
					new String[][]{
						{"3", "3", "<\"\">", "<0>"},
					}, 
					new String[][]{
						{"3", "2"},
						{"3", "1"},
						{"3", "0"}
					}
				},
				{
					split(3),
					new String[][]{
						{"1", "! 1", "<\"\">", "<0>"},
						{"2", "! 2", "<\"\">", "<0>"},
						{"3", "! 3", "<\"\">", "<0>"},
					},
					new String[][]{
					}
				},
				{
					splitControl(3),
					new String[][]{
						{"1", "1", "<\"\">", "<0>"},
						{"2", "2", "<\"\">", "<0>"},
						{"3", "3", "<\"\">", "<0>"},
					},
					new String[][]{
						{"1", "!"},
						{"2", "!"},
						{"3", "!"},
					}
				},
				{
					mergeMixedDataControl(4),
					new String[][]{
						{"4", "1", "<\"\">", "<0>"},
						{"4", "3", "<\"\">", "<0>"},
					},
					new String[][]{
						{"4", "0"},
						{"4", "2"}
					}
				},
				{
					sequence3bisWithAdaptiveness(3),
					// contains out
					new String[][]{
						{"2", "3", "<\"\">", "<0>"},
						{"2", "4", "<\"\">", "<0>"},
						{"2", "5", "<\"\">", "<0>"}
					},
					new String[][]{},
				},
				{
					sequenceControl6With2Adaptiveness(),
					new String[][]{
						{"1", "", "", "<127>"}, // these two fail
						{"4", "", "", "<127>"},
						{"5", "", "", "<0>"} // but the wf ends
					},
					new String[][]{}
				}
				
		};
		return tests;
	}

	protected Object[][] provideAdaptiveness() {
		Object[][] tests =  new Object[][]{
			{
				sequenceControlWithUpdate(),
				new String[][] {
					{"2", "", "", "<127>"}, // 2 is replaced by 4
					{"4", "4", "", "<0>"}, 	// 4 is executed
					{"3", "", "", "<0>"} 	// 3 is the final result
				},
				new String[][] {}
			}
		};
		return tests;
	}

	protected static Options createOptions(String executor, String broker) throws OptionException, FileNotFoundException {
		Options options = new Options();
		// load the right option file
		try {
			options = Options.fromFile(String.format("src/test/resources/ginflow-%s.yaml", executor));
		} catch (FileNotFoundException exception) {
			// the file isn't found (this should be the case when running in ci.inria.fr)
			// don't do anything just use the default parameters.
		}
		
		// override broker
		options.put(Options.BROKER, broker);
		return options;
	}

    
}
