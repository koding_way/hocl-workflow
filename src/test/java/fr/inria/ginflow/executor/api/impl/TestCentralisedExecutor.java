/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.executor.api.impl;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import fr.inria.ginflow.executor.ExecutorFactory;
import fr.inria.ginflow.executor.api.GinflowExecutor;
import fr.inria.ginflow.executor.api.impl.CentralisedExecutor;
import fr.inria.ginflow.internal.api.GinflowTuple;
import fr.inria.ginflow.internal.api.Service;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.options.Options;

/**
 * @author msimonin
 * 
 */
@Test(groups = { "main", "central" })
public class TestCentralisedExecutor extends TestExecutor {
    
    @Test(enabled = true, groups = {"central1"})
    public void test_workflow_sequence() throws Exception {
        // Exclamation topology ?
        int max = 2; 
        Workflow workflow = sequence(max, "echo !");
        GinflowExecutor executor = new CentralisedExecutor(workflow, new Options());
        executor.execute();
        // get last result
        Service service = workflow.getService(String.valueOf(max - 1));
        // parsing result.
        GinflowTuple[] result = service.getResult();
        // testing results
        Assert.assertNotNull(result);
        Assert.assertEquals(result[Service.OUT_INDEX].getValue().toString(), "<\"! !\">");
        Assert.assertEquals(result[Service.ERR_INDEX].getValue().toString(), "<\"\">");
        Assert.assertEquals(result[Service.EXIT_INDEX].getValue().toString(), "<0>");
    }
    
    @Test(enabled = true)
    public void test_workflow_sequence_control() throws Exception {
        // Exclamation topology ?
        int max = 2; 
        Workflow workflow = sequenceControl(max, "echo !");
        GinflowExecutor executor = new CentralisedExecutor(workflow, new Options());
        System.out.println(workflow);
        executor.execute();
        System.out.println(workflow);
        // get last result
        Service service = workflow.getService(String.valueOf(max - 1));
        // parsing result.
        GinflowTuple[] result = service.getResult();
        // testing results
        Assert.assertNotNull(result);
        Assert.assertEquals(result[Service.OUT_INDEX].getValue().toString(), "<\"!\">");
        Assert.assertEquals(result[Service.ERR_INDEX].getValue().toString(), "<\"\">");
        Assert.assertEquals(result[Service.EXIT_INDEX].getValue().toString(), "<0>");
    }
    
    
    @Test(enabled = true)
    public void test_workflow_merge() throws Exception {
        int max = 3;
        Workflow workflow = merge(max);
        GinflowExecutor executor = new CentralisedExecutor(workflow, new Options());
        executor.execute();
        // get last result
        Service service = workflow.getService(String.valueOf(max));
        // parsing result.
        GinflowTuple[] result = service.getResult();
        
        // testing results (out:<3* 2* 1* O*>)
        Assert.assertNotNull(result);
        Assert.assertTrue(result[Service.OUT_INDEX].getValue().toString().contains("3"));
        Assert.assertTrue(result[Service.OUT_INDEX].getValue().toString().contains("2"));
        Assert.assertTrue(result[Service.OUT_INDEX].getValue().toString().contains("1"));
        Assert.assertTrue(result[Service.OUT_INDEX].getValue().toString().contains("0"));
        Assert.assertEquals(result[Service.ERR_INDEX].getValue().toString(), "<\"\">");
        Assert.assertEquals(result[Service.EXIT_INDEX].getValue().toString(), "<0>");
    }
    
    @Test(enabled = true)
    public void test_workflow_merge_control() throws Exception {
        int max = 3;
        Workflow workflow = mergeControl(max);
        GinflowExecutor executor = new CentralisedExecutor(workflow, new Options());
        executor.execute();
        // get last result
        Service service = workflow.getService(String.valueOf(max));
        // parsing result.
        GinflowTuple[] result = service.getResult();
        
        // testing results (out:<3*>)
        Assert.assertNotNull(result);
        Assert.assertTrue(result[Service.OUT_INDEX].getValue().toString().contains("3"));
        Assert.assertFalse(result[Service.OUT_INDEX].getValue().toString().contains("2"));
        Assert.assertFalse(result[Service.OUT_INDEX].getValue().toString().contains("1"));
        Assert.assertFalse(result[Service.OUT_INDEX].getValue().toString().contains("0"));
        Assert.assertEquals(result[Service.ERR_INDEX].getValue().toString(), "<\"\">");
        Assert.assertEquals(result[Service.EXIT_INDEX].getValue().toString(), "<0>");
    }
    
    @Test(enabled = true)
    public void test_workflow_split() throws Exception {
        int max = 3;
        Workflow workflow = split(max);
        GinflowExecutor executor = new CentralisedExecutor(workflow, new Options());
        executor.execute();
        
        for (String sName : workflow.getTerminalServices()) {
            Service service = workflow.getService(sName);
            // parsing result.
            GinflowTuple[] result = service.getResult();
            
            // testing results (out:<! i >)
            Assert.assertTrue(result[Service.OUT_INDEX].getValue().toString().contains("!"));
            Assert.assertTrue(result[Service.OUT_INDEX].getValue().toString().contains(sName));
            Assert.assertEquals(result[Service.ERR_INDEX].getValue().toString(), "<\"\">");
            Assert.assertEquals(result[Service.EXIT_INDEX].getValue().toString(), "<0>");
        }
    }
    
    @Test(enabled = true)
    public void test_workflow_split_control() throws Exception {
        int max = 3;
        Workflow workflow = splitControl(max);
        GinflowExecutor executor = new CentralisedExecutor(workflow, new Options());
        executor.execute();
        
        for (String sName : workflow.getTerminalServices()) {
            Service service = workflow.getService(sName);
            // parsing result.
            GinflowTuple[] result = service.getResult();
            // testing results (out:<! i >)
            Assert.assertFalse(result[Service.OUT_INDEX].getValue().toString().contains("!"));
            Assert.assertTrue(result[Service.OUT_INDEX].getValue().toString().contains(sName));
            Assert.assertEquals(result[Service.ERR_INDEX].getValue().toString(), "<\"\">");
            Assert.assertEquals(result[Service.EXIT_INDEX].getValue().toString(), "<0>");
        }
    }
    
    @Test(enabled = true)
    public void test_workflow_merge_mixed() throws Exception {
        int max = 3;
        Workflow workflow = mergeMixedDataControl(max);
        GinflowExecutor executor = new CentralisedExecutor(workflow, new Options());
        executor.execute();
        // get last result
        Service service = workflow.getService(String.valueOf(max));
        // parsing result.
        GinflowTuple[] result = service.getResult();
        
        // testing results (out:<3*>)
        Assert.assertNotNull(result);
        Assert.assertTrue(result[Service.OUT_INDEX].getValue().toString().contains("3"));
        Assert.assertFalse(result[Service.OUT_INDEX].getValue().toString().contains("2"));
        Assert.assertTrue(result[Service.OUT_INDEX].getValue().toString().contains("1"));
        Assert.assertFalse(result[Service.OUT_INDEX].getValue().toString().contains("0"));
        Assert.assertEquals(result[Service.ERR_INDEX].getValue().toString(), "<\"\">");
        Assert.assertEquals(result[Service.EXIT_INDEX].getValue().toString(), "<0>");
    }
    
    
}
