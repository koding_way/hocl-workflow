/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.executor.api.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import fr.inria.ginflow.distributed.deployer.api.Command;
import fr.inria.ginflow.distributed.deployer.api.impl.ssh.GanySshRemoteExecutor;
import fr.inria.ginflow.distributed.deployer.api.impl.ssh.RemoteExecutor;
import fr.inria.ginflow.options.OptionException;
import fr.inria.ginflow.options.Options;

/**
 * @author msimonin
 * 
 */
@Test(groups = { "integration" })
public class ITGanySSHRemoteExecutor extends TestExecutor {
    

    private static Map<String, String> options_;

    @BeforeClass
    public static void setUpClass() throws OptionException, FileNotFoundException {
    	
    	Options options = createOptions("ssh", "activemq");
    }
    
    // Unknown host exception
    @Test(enabled = true, expectedExceptions = Exception.class)
    public void test_one_host_one_unkown() throws Exception {
        
        try (RemoteExecutor remoteExecutor = new GanySshRemoteExecutor(
                options_.get(Options.SSH_USERNAME),
                new File(options_.get(Options.SSH_PRIVATE_KEY)), 
                options_.get(Options.SSH_PASSWORD),
                null
        		)){
            remoteExecutor.exec("unkownHost", new Command("date"));
        }
    }

    // Unknown host exception
    @Test(enabled = true, expectedExceptions = Exception.class)
    public void test_two_hosts_one_unkown() throws Exception {
        
        try (RemoteExecutor remoteExecutor = new GanySshRemoteExecutor(
                options_.get(Options.SSH_USERNAME),
                new File(options_.get(Options.SSH_PRIVATE_KEY)), 
                options_.get(Options.SSH_PASSWORD),
                null)){
            remoteExecutor.execParallel(Arrays.asList("localhost", "unkownHost"), new Command("date"));
        }
    }
    
    

}
