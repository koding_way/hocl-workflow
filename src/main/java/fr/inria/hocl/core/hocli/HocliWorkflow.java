/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.hocl.core.hocli;

/**
 * 
 * HOCL interpreter
 * 
 * It reduces a given solution to a inert state. Strategies and debug messages
 * may be specified here.
 * 
 * Command line switches: [-s [FR|UFR|Rand|KSS]] [-v [n]]
 * 
 * [Strategies] A strategy to apply the rules may be specified between : - FR :
 * Fair Roundrobin (R1;R2;...;Rn)* - UFR : UnFair Roudrobin (R1*;R2*;...;Rn*)* -
 * Rand : select randomly a rule between the non inert ones - KSS : Keep Small
 * Solution, favorise Reducer over Optimiser over Unknown over Expanser
 * 
 * 
 * [Debugging] Verbose levels (each level assumes all lower ones): 
 * <ul>
 * <li>0 : quiet</li> 
 * <li>1 : initial and final inert states</li>
 * <li>2 : all inert solutions</li>
 * <li>3 : reactions</li>
 * <li>4 : reactions statistics</li>
 * <li>5 : intermediate solutions (non inert) states </li>
 * <li>6 : tested permutations</li>
 * <li>7 : strategy choices</li>
 * <li>8 : all permutations</li>
 * </ul>
 * 
 * By default, everything is turned off execept the initial state (argument) and
 * the last state (result).
 * 
 */
public class HocliWorkflow extends Hocli {

    /** The optional workflow id whose service is taking part. */
    static public String workflowId = "multiset";

    /** The optional service id. */
    static public String serviceId = "";

    /**
     * The optional topic where result could be published (see generated
     * NoCliClass) Mset side only. On service side, it could maybe used to send
     * notifications about the invocation status ?
     * */
    static public String resultTopic = "result";

    /**
     * broker url (introduced in nextGen)
     */
    static public String brokerUrl = "tcp://localhost:61616";

    /**
     * Init the interpretor to defaults and with the command line options
     * 
     * @param args
     *            command line options
     */
    static public void init(final String args[]) {
        initStrategies();
        setStrategy("FR");
        initDebugMessages();
        analyzeArguments(args);
    }

    /**
     * Analyze the command line options to extract the optional specified
     * strategy and debug level.
     * 
     * 
     * @param args
     *            options to analyze
     */
    static private void analyzeArguments(String args[]) {
        int size = args.length;
        for (int i = 0; i < size; i++) {
            if (args[i].equals("-s")) {
                i++;
                if (i >= size || args[i].charAt(0) == '-') {
                    stopError("Missing strategy after '-s'. May be one of:\n"
                            + allStrategies());
                } else {
                    Class<?> strat = allStrategies.get(args[i]);
                    if (strat == null) {
                        stopError("Unknown strategy '" + args[i]
                                + "'. It should be one of:\n" + allStrategies());
                    } else {
                        strategyTag = args[i];
                    }
                }
            } else if (args[i].equals("-v")) {
                int verb = 1;
                i++;
                if (i < size && args[i].charAt(0) != '-') {
                    try {
                        verb = Integer.decode(args[i]);
                    } catch (NumberFormatException ex) {
                        stopError("Non valid verbose level: " + ex.getMessage());
                    }
                }
                setVerboseLevel(verb);
                // workflow id
            } else if (args[i].equals("-w")) {
                i++;
                if (i >= size || args[i].charAt(0) == '-') {
                    stopError("Missing workflow id after '-w'");
                } else {
                    workflowId = args[i];
                    // setting the workflow id sets the result topic aswell
                    // Mset side only
                    // Matt : may be it could be use to send service invocation
                    // status ?
                    resultTopic = "results." + workflowId;
                }
                // service name
            } else if (args[i].equals("-n")) {
                i++;
                if (i >= size || args[i].charAt(0) == '-') {
                    stopError("Missing workflow id after '-w'");
                } else {
                    serviceId = args[i];
                }
                // broker url
            } else if (args[i].equals("-b")) {
                i++;
                if (i >= size || args[i].charAt(0) == '-') {
                    stopError("Missing broker url after '-b'");
                } else {
                    brokerUrl = args[i];
                }
            } else {
                // stopError( "Unknown argument: " + args[i] );
                // don't stop on error.
            }
        }
    }

} // class Hocli
