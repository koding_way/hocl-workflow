/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.rules.centralised;
import fr.inria.ginflow.invoker.Invoker;
import fr.inria.hocl.core.hocli.*;
import java.io.*;

public class Gw_pass_control extends ReactionRule implements Serializable {

	
	public Gw_pass_control(){
		super("gw_pass_control", Shot.N_SHOT);
		setTrope(Trope.OPTIMIZER);
			AtomIterator[] _HOCL_atomIteratorArray21;
		_HOCL_atomIteratorArray21 = new AtomIterator[2];
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple18;
			_HOCL_atomIteratorArrayTuple18 = new AtomIterator[2];
			{
				class AtomIterator_t_j extends IteratorForExternal {
					public AtomIterator_t_j(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							satisfied = true;
						}
						return satisfied;
					}
				
				} // end of class AtomIterator_t_j
				_HOCL_atomIteratorArrayTuple18[0] = new AtomIterator_t_j();
			}
			{
				class IteratorSolution39 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray21;
						_HOCL_atomIteratorArray21 = new AtomIterator[1];
						{
							AtomIterator[] _HOCL_atomIteratorArrayTuple19;
							_HOCL_atomIteratorArrayTuple19 = new AtomIterator[2];
							{
								class AtomIterator__HOCL_literal16 extends IteratorForExternal {
									public AtomIterator__HOCL_literal16(){
										access = Access.REWRITE;
									}
									@Override
									public boolean conditionSatisfied() {
										Atom atom;
										boolean satisfied;
										atom = iterator.getElement();
										satisfied = false;
										if (atom instanceof ExternalObject
										  && ((ExternalObject)atom).getObject() instanceof String) {
											
											IteratorForTuple _HOCL_tupleAtomIterator65 = (IteratorForTuple)permutation.getAtomIterator(0);
											IteratorForSolution _HOCL_iteratorSolution46 = (IteratorForSolution)_HOCL_tupleAtomIterator65.getAtomIterator(1);
											IteratorForTuple _HOCL_tupleAtomIterator66 = (IteratorForTuple)_HOCL_iteratorSolution46.getSubPermutation().getAtomIterator(0);
											String _HOCL_literal16 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator66.getAtomIterator(0)).getObject();
											satisfied = _HOCL_literal16.equals("SRC_CONTROL");
										}
										return satisfied;
									}
								
								} // end of class AtomIterator__HOCL_literal16
								_HOCL_atomIteratorArrayTuple19[0] = new AtomIterator__HOCL_literal16();
							}
							{
								class IteratorSolution37 extends IteratorForSolution {
									protected Permutation makeSubPermutation(){
										Permutation perm;
										AtomIterator[] _HOCL_atomIteratorArray21;
										_HOCL_atomIteratorArray21 = new AtomIterator[1];
										{
											class AtomIterator_t_i extends IteratorForExternal {
												public AtomIterator_t_i(){
													access = Access.REWRITE;
												}
												@Override
												public boolean conditionSatisfied() {
													Atom atom;
													boolean satisfied;
													atom = iterator.getElement();
													satisfied = false;
													if (atom instanceof ExternalObject
													  && ((ExternalObject)atom).getObject() instanceof String) {
														satisfied = true;
													}
													return satisfied;
												}
											
											} // end of class AtomIterator_t_i
											_HOCL_atomIteratorArray21[0] = new AtomIterator_t_i();
										}
										MoleculeIterator[] _HOCL_moleculeIteratorArray21 = new MoleculeIterator[1];
										_HOCL_moleculeIteratorArray21[0] = new MoleculeIterator(1); // w_src
										
										perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray21, _HOCL_moleculeIteratorArray21);
										return perm;
									}
								
								} // class IteratorSolution37
								_HOCL_atomIteratorArrayTuple19[1] = new IteratorSolution37();
							}
							_HOCL_atomIteratorArray21[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple19, Gw_pass_control.this);
						}
						MoleculeIterator[] _HOCL_moleculeIteratorArray22 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray22[0] = new MoleculeIterator(1); // w_j
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray21, _HOCL_moleculeIteratorArray22);
						return perm;
					}
				
				} // class IteratorSolution39
				_HOCL_atomIteratorArrayTuple18[1] = new IteratorSolution39();
			}
			_HOCL_atomIteratorArray21[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple18, Gw_pass_control.this);
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple20;
			_HOCL_atomIteratorArrayTuple20 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_t_i_copy0 extends IteratorForExternal {
					public AtomIterator__HOCL_t_i_copy0(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator67 = (IteratorForTuple)permutation.getAtomIterator(0);
							IteratorForSolution _HOCL_iteratorSolution47 = (IteratorForSolution)_HOCL_tupleAtomIterator67.getAtomIterator(1);
							IteratorForTuple _HOCL_tupleAtomIterator68 = (IteratorForTuple)_HOCL_iteratorSolution47.getSubPermutation().getAtomIterator(0);
							IteratorForSolution _HOCL_iteratorSolution48 = (IteratorForSolution)_HOCL_tupleAtomIterator68.getAtomIterator(1);
							String t_i = (String)((IteratorForExternal)_HOCL_iteratorSolution48.getSubPermutation().getAtomIterator(0)).getObject();
							IteratorForTuple _HOCL_tupleAtomIterator69 = (IteratorForTuple)permutation.getAtomIterator(1);
							String _HOCL_t_i_copy0 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator69.getAtomIterator(0)).getObject();
							satisfied = _HOCL_t_i_copy0.equals(t_i);
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_t_i_copy0
				_HOCL_atomIteratorArrayTuple20[0] = new AtomIterator__HOCL_t_i_copy0();
			}
			{
				class IteratorSolution51 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray23;
						_HOCL_atomIteratorArray23 = new AtomIterator[2];
						{
							AtomIterator[] _HOCL_atomIteratorArrayTuple21;
							_HOCL_atomIteratorArrayTuple21 = new AtomIterator[2];
							{
								class AtomIterator__HOCL_literal17 extends IteratorForExternal {
									public AtomIterator__HOCL_literal17(){
										access = Access.REWRITE;
									}
									@Override
									public boolean conditionSatisfied() {
										Atom atom;
										boolean satisfied;
										atom = iterator.getElement();
										satisfied = false;
										if (atom instanceof ExternalObject
										  && ((ExternalObject)atom).getObject() instanceof String) {
											
											IteratorForTuple _HOCL_tupleAtomIterator70 = (IteratorForTuple)permutation.getAtomIterator(1);
											IteratorForSolution _HOCL_iteratorSolution49 = (IteratorForSolution)_HOCL_tupleAtomIterator70.getAtomIterator(1);
											IteratorForTuple _HOCL_tupleAtomIterator71 = (IteratorForTuple)_HOCL_iteratorSolution49.getSubPermutation().getAtomIterator(0);
											String _HOCL_literal17 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator71.getAtomIterator(0)).getObject();
											satisfied = _HOCL_literal17.equals("RES");
										}
										return satisfied;
									}
								
								} // end of class AtomIterator__HOCL_literal17
								_HOCL_atomIteratorArrayTuple21[0] = new AtomIterator__HOCL_literal17();
							}
							{
								class IteratorSolution47 extends IteratorForSolution {
									protected Permutation makeSubPermutation(){
										Permutation perm;
										AtomIterator[] _HOCL_atomIteratorArray23;
										_HOCL_atomIteratorArray23 = new AtomIterator[1];
										{
											AtomIterator[] _HOCL_atomIteratorArrayTuple22;
											_HOCL_atomIteratorArrayTuple22 = new AtomIterator[2];
											{
												class AtomIterator__HOCL_literal18 extends IteratorForExternal {
													public AtomIterator__HOCL_literal18(){
														access = Access.REWRITE;
													}
													@Override
													public boolean conditionSatisfied() {
														Atom atom;
														boolean satisfied;
														atom = iterator.getElement();
														satisfied = false;
														if (atom instanceof ExternalObject
														  && ((ExternalObject)atom).getObject() instanceof String) {
															
															IteratorForTuple _HOCL_tupleAtomIterator72 = (IteratorForTuple)permutation.getAtomIterator(1);
															IteratorForSolution _HOCL_iteratorSolution50 = (IteratorForSolution)_HOCL_tupleAtomIterator72.getAtomIterator(1);
															IteratorForTuple _HOCL_tupleAtomIterator73 = (IteratorForTuple)_HOCL_iteratorSolution50.getSubPermutation().getAtomIterator(0);
															IteratorForSolution _HOCL_iteratorSolution51 = (IteratorForSolution)_HOCL_tupleAtomIterator73.getAtomIterator(1);
															IteratorForTuple _HOCL_tupleAtomIterator74 = (IteratorForTuple)_HOCL_iteratorSolution51.getSubPermutation().getAtomIterator(0);
															String _HOCL_literal18 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator74.getAtomIterator(0)).getObject();
															satisfied = _HOCL_literal18.equals("TRANSFER");
														}
														return satisfied;
													}
												
												} // end of class AtomIterator__HOCL_literal18
												_HOCL_atomIteratorArrayTuple22[0] = new AtomIterator__HOCL_literal18();
											}
											{
												class IteratorSolution45 extends IteratorForSolution {
													protected Permutation makeSubPermutation(){
														Permutation perm;
														AtomIterator[] _HOCL_atomIteratorArray23;
														_HOCL_atomIteratorArray23 = new AtomIterator[2];
														{
															AtomIterator[] _HOCL_atomIteratorArrayTuple23;
															_HOCL_atomIteratorArrayTuple23 = new AtomIterator[2];
															{
																class AtomIterator__HOCL_literal19 extends IteratorForExternal {
																	public AtomIterator__HOCL_literal19(){
																		access = Access.REWRITE;
																	}
																	@Override
																	public boolean conditionSatisfied() {
																		Atom atom;
																		boolean satisfied;
																		atom = iterator.getElement();
																		satisfied = false;
																		if (atom instanceof ExternalObject
																		  && ((ExternalObject)atom).getObject() instanceof String) {
																			
																			IteratorForTuple _HOCL_tupleAtomIterator75 = (IteratorForTuple)permutation.getAtomIterator(1);
																			IteratorForSolution _HOCL_iteratorSolution52 = (IteratorForSolution)_HOCL_tupleAtomIterator75.getAtomIterator(1);
																			IteratorForTuple _HOCL_tupleAtomIterator76 = (IteratorForTuple)_HOCL_iteratorSolution52.getSubPermutation().getAtomIterator(0);
																			IteratorForSolution _HOCL_iteratorSolution53 = (IteratorForSolution)_HOCL_tupleAtomIterator76.getAtomIterator(1);
																			IteratorForTuple _HOCL_tupleAtomIterator77 = (IteratorForTuple)_HOCL_iteratorSolution53.getSubPermutation().getAtomIterator(0);
																			IteratorForSolution _HOCL_iteratorSolution54 = (IteratorForSolution)_HOCL_tupleAtomIterator77.getAtomIterator(1);
																			IteratorForTuple _HOCL_tupleAtomIterator78 = (IteratorForTuple)_HOCL_iteratorSolution54.getSubPermutation().getAtomIterator(0);
																			String _HOCL_literal19 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator78.getAtomIterator(0)).getObject();
																			satisfied = _HOCL_literal19.equals("OUT");
																		}
																		return satisfied;
																	}
																
																} // end of class AtomIterator__HOCL_literal19
																_HOCL_atomIteratorArrayTuple23[0] = new AtomIterator__HOCL_literal19();
															}
															{
																class IteratorSolution41 extends IteratorForSolution {
																	protected Permutation makeSubPermutation(){
																		Permutation perm;
																		AtomIterator[] _HOCL_atomIteratorArray23;
																		_HOCL_atomIteratorArray23 = new AtomIterator[1];
																		{
																			class AtomIterator_out extends IteratorForExternal {
																				public AtomIterator_out(){
																					access = Access.REWRITE;
																				}
																				@Override
																				public boolean conditionSatisfied() {
																					Atom atom;
																					boolean satisfied;
																					atom = iterator.getElement();
																					satisfied = false;
																					if (atom instanceof ExternalObject
																					  && ((ExternalObject)atom).getObject() instanceof String) {
																						satisfied = true;
																					}
																					return satisfied;
																				}
																			
																			} // end of class AtomIterator_out
																			_HOCL_atomIteratorArray23[0] = new AtomIterator_out();
																		}
																		MoleculeIterator[] _HOCL_moleculeIteratorArray23 = new MoleculeIterator[0];
																		perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray23, _HOCL_moleculeIteratorArray23);
																		return perm;
																	}
																
																} // class IteratorSolution41
																_HOCL_atomIteratorArrayTuple23[1] = new IteratorSolution41();
															}
															_HOCL_atomIteratorArray23[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple23, Gw_pass_control.this);
														}
														{
															AtomIterator[] _HOCL_atomIteratorArrayTuple24;
															_HOCL_atomIteratorArrayTuple24 = new AtomIterator[2];
															{
																class AtomIterator__HOCL_literal20 extends IteratorForExternal {
																	public AtomIterator__HOCL_literal20(){
																		access = Access.REWRITE;
																	}
																	@Override
																	public boolean conditionSatisfied() {
																		Atom atom;
																		boolean satisfied;
																		atom = iterator.getElement();
																		satisfied = false;
																		if (atom instanceof ExternalObject
																		  && ((ExternalObject)atom).getObject() instanceof String) {
																			
																			IteratorForTuple _HOCL_tupleAtomIterator79 = (IteratorForTuple)permutation.getAtomIterator(1);
																			IteratorForSolution _HOCL_iteratorSolution55 = (IteratorForSolution)_HOCL_tupleAtomIterator79.getAtomIterator(1);
																			IteratorForTuple _HOCL_tupleAtomIterator80 = (IteratorForTuple)_HOCL_iteratorSolution55.getSubPermutation().getAtomIterator(0);
																			IteratorForSolution _HOCL_iteratorSolution56 = (IteratorForSolution)_HOCL_tupleAtomIterator80.getAtomIterator(1);
																			IteratorForTuple _HOCL_tupleAtomIterator81 = (IteratorForTuple)_HOCL_iteratorSolution56.getSubPermutation().getAtomIterator(0);
																			IteratorForSolution _HOCL_iteratorSolution57 = (IteratorForSolution)_HOCL_tupleAtomIterator81.getAtomIterator(1);
																			IteratorForTuple _HOCL_tupleAtomIterator82 = (IteratorForTuple)_HOCL_iteratorSolution57.getSubPermutation().getAtomIterator(1);
																			String _HOCL_literal20 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator82.getAtomIterator(0)).getObject();
																			satisfied = _HOCL_literal20.equals("EXIT");
																		}
																		return satisfied;
																	}
																
																} // end of class AtomIterator__HOCL_literal20
																_HOCL_atomIteratorArrayTuple24[0] = new AtomIterator__HOCL_literal20();
															}
															{
																class IteratorSolution43 extends IteratorForSolution {
																	protected Permutation makeSubPermutation(){
																		Permutation perm;
																		AtomIterator[] _HOCL_atomIteratorArray24;
																		_HOCL_atomIteratorArray24 = new AtomIterator[1];
																		{
																			class AtomIterator_exit extends IteratorForExternal {
																				public AtomIterator_exit(){
																					access = Access.REWRITE;
																				}
																				@Override
																				public boolean conditionSatisfied() {
																					Atom atom;
																					boolean satisfied;
																					atom = iterator.getElement();
																					satisfied = false;
																					if (atom instanceof ExternalObject
																					  && ((ExternalObject)atom).getObject() instanceof Integer) {
																						satisfied = true;
																					}
																					return satisfied;
																				}
																			
																			} // end of class AtomIterator_exit
																			_HOCL_atomIteratorArray24[0] = new AtomIterator_exit();
																		}
																		MoleculeIterator[] _HOCL_moleculeIteratorArray24 = new MoleculeIterator[0];
																		perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray24, _HOCL_moleculeIteratorArray24);
																		return perm;
																	}
																
																} // class IteratorSolution43
																_HOCL_atomIteratorArrayTuple24[1] = new IteratorSolution43();
															}
															_HOCL_atomIteratorArray23[1] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple24, Gw_pass_control.this);
														}
														MoleculeIterator[] _HOCL_moleculeIteratorArray25 = new MoleculeIterator[1];
														_HOCL_moleculeIteratorArray25[0] = new MoleculeIterator(1); // w_res
														
														perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray23, _HOCL_moleculeIteratorArray25);
														return perm;
													}
												
												} // class IteratorSolution45
												_HOCL_atomIteratorArrayTuple22[1] = new IteratorSolution45();
											}
											_HOCL_atomIteratorArray23[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple22, Gw_pass_control.this);
										}
										MoleculeIterator[] _HOCL_moleculeIteratorArray26 = new MoleculeIterator[0];
										perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray23, _HOCL_moleculeIteratorArray26);
										return perm;
									}
								
								} // class IteratorSolution47
								_HOCL_atomIteratorArrayTuple21[1] = new IteratorSolution47();
							}
							_HOCL_atomIteratorArray23[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple21, Gw_pass_control.this);
						}
						{
							AtomIterator[] _HOCL_atomIteratorArrayTuple25;
							_HOCL_atomIteratorArrayTuple25 = new AtomIterator[2];
							{
								class AtomIterator__HOCL_literal21 extends IteratorForExternal {
									public AtomIterator__HOCL_literal21(){
										access = Access.REWRITE;
									}
									@Override
									public boolean conditionSatisfied() {
										Atom atom;
										boolean satisfied;
										atom = iterator.getElement();
										satisfied = false;
										if (atom instanceof ExternalObject
										  && ((ExternalObject)atom).getObject() instanceof String) {
											
											IteratorForTuple _HOCL_tupleAtomIterator83 = (IteratorForTuple)permutation.getAtomIterator(1);
											IteratorForSolution _HOCL_iteratorSolution58 = (IteratorForSolution)_HOCL_tupleAtomIterator83.getAtomIterator(1);
											IteratorForTuple _HOCL_tupleAtomIterator84 = (IteratorForTuple)_HOCL_iteratorSolution58.getSubPermutation().getAtomIterator(1);
											String _HOCL_literal21 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator84.getAtomIterator(0)).getObject();
											satisfied = _HOCL_literal21.equals("DST_CONTROL");
										}
										return satisfied;
									}
								
								} // end of class AtomIterator__HOCL_literal21
								_HOCL_atomIteratorArrayTuple25[0] = new AtomIterator__HOCL_literal21();
							}
							{
								class IteratorSolution49 extends IteratorForSolution {
									protected Permutation makeSubPermutation(){
										Permutation perm;
										AtomIterator[] _HOCL_atomIteratorArray27;
										_HOCL_atomIteratorArray27 = new AtomIterator[1];
										{
											class AtomIterator__HOCL_t_j_copy0 extends IteratorForExternal {
												public AtomIterator__HOCL_t_j_copy0(){
													access = Access.REWRITE;
												}
												@Override
												public boolean conditionSatisfied() {
													Atom atom;
													boolean satisfied;
													atom = iterator.getElement();
													satisfied = false;
													if (atom instanceof ExternalObject
													  && ((ExternalObject)atom).getObject() instanceof String) {
														
														IteratorForTuple _HOCL_tupleAtomIterator85 = (IteratorForTuple)permutation.getAtomIterator(1);
														IteratorForSolution _HOCL_iteratorSolution59 = (IteratorForSolution)_HOCL_tupleAtomIterator85.getAtomIterator(1);
														IteratorForTuple _HOCL_tupleAtomIterator86 = (IteratorForTuple)_HOCL_iteratorSolution59.getSubPermutation().getAtomIterator(1);
														IteratorForSolution _HOCL_iteratorSolution60 = (IteratorForSolution)_HOCL_tupleAtomIterator86.getAtomIterator(1);
														String _HOCL_t_j_copy0 = (String)((IteratorForExternal)_HOCL_iteratorSolution60.getSubPermutation().getAtomIterator(0)).getObject();
														IteratorForTuple _HOCL_tupleAtomIterator87 = (IteratorForTuple)permutation.getAtomIterator(1);
														IteratorForSolution _HOCL_iteratorSolution61 = (IteratorForSolution)_HOCL_tupleAtomIterator87.getAtomIterator(1);
														IteratorForTuple _HOCL_tupleAtomIterator88 = (IteratorForTuple)_HOCL_iteratorSolution61.getSubPermutation().getAtomIterator(0);
														IteratorForSolution _HOCL_iteratorSolution62 = (IteratorForSolution)_HOCL_tupleAtomIterator88.getAtomIterator(1);
														IteratorForTuple _HOCL_tupleAtomIterator89 = (IteratorForTuple)_HOCL_iteratorSolution62.getSubPermutation().getAtomIterator(0);
														IteratorForSolution _HOCL_iteratorSolution63 = (IteratorForSolution)_HOCL_tupleAtomIterator89.getAtomIterator(1);
														IteratorForTuple _HOCL_tupleAtomIterator90 = (IteratorForTuple)_HOCL_iteratorSolution63.getSubPermutation().getAtomIterator(1);
														IteratorForSolution _HOCL_iteratorSolution64 = (IteratorForSolution)_HOCL_tupleAtomIterator90.getAtomIterator(1);
														Integer exit = (Integer)((IteratorForExternal)_HOCL_iteratorSolution64.getSubPermutation().getAtomIterator(0)).getObject();
														IteratorForTuple _HOCL_tupleAtomIterator91 = (IteratorForTuple)permutation.getAtomIterator(0);
														String t_j = (String)((IteratorForExternal)_HOCL_tupleAtomIterator91.getAtomIterator(0)).getObject();
														satisfied = _HOCL_t_j_copy0.equals(t_j) && (exit == 0);
													}
													return satisfied;
												}
											
											} // end of class AtomIterator__HOCL_t_j_copy0
											_HOCL_atomIteratorArray27[0] = new AtomIterator__HOCL_t_j_copy0();
										}
										MoleculeIterator[] _HOCL_moleculeIteratorArray27 = new MoleculeIterator[1];
										_HOCL_moleculeIteratorArray27[0] = new MoleculeIterator(1); // w_dst
										
										perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray27, _HOCL_moleculeIteratorArray27);
										return perm;
									}
								
								} // class IteratorSolution49
								_HOCL_atomIteratorArrayTuple25[1] = new IteratorSolution49();
							}
							_HOCL_atomIteratorArray23[1] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple25, Gw_pass_control.this);
						}
						MoleculeIterator[] _HOCL_moleculeIteratorArray28 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray28[0] = new MoleculeIterator(1); // w_i
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray23, _HOCL_moleculeIteratorArray28);
						return perm;
					}
				
				} // class IteratorSolution51
				_HOCL_atomIteratorArrayTuple20[1] = new IteratorSolution51();
			}
			_HOCL_atomIteratorArray21[1] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple20, Gw_pass_control.this);
		}
		MoleculeIterator[] _HOCL_moleculeIteratorArray29 = new MoleculeIterator[0];
		permutation = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray21, _HOCL_moleculeIteratorArray29);
	}
	public Gw_pass_control clone() {
		 return new Gw_pass_control();
	}

	public void addType(String s){}

	// compute result of the rule gw_pass_control
	protected Molecule computeResult(){
		ExternalObject object;
		ReactionRule rule;
		String[] string;
		Tuple tuple;
		HoclList list;		
		IteratorForTuple _HOCL_tupleAtomIterator92 = (IteratorForTuple)permutation.getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution65 = (IteratorForSolution)_HOCL_tupleAtomIterator92.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator93 = (IteratorForTuple)_HOCL_iteratorSolution65.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution66 = (IteratorForSolution)_HOCL_tupleAtomIterator93.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator94 = (IteratorForTuple)_HOCL_iteratorSolution66.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution67 = (IteratorForSolution)_HOCL_tupleAtomIterator94.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator95 = (IteratorForTuple)_HOCL_iteratorSolution67.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution68 = (IteratorForSolution)_HOCL_tupleAtomIterator95.getAtomIterator(1);
		String out = (String)((IteratorForExternal)_HOCL_iteratorSolution68.getSubPermutation().getAtomIterator(0)).getObject();
		IteratorForTuple _HOCL_tupleAtomIterator96 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution69 = (IteratorForSolution)_HOCL_tupleAtomIterator96.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator97 = (IteratorForTuple)_HOCL_iteratorSolution69.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution70 = (IteratorForSolution)_HOCL_tupleAtomIterator97.getAtomIterator(1);
		String t_i = (String)((IteratorForExternal)_HOCL_iteratorSolution70.getSubPermutation().getAtomIterator(0)).getObject();
		IteratorForTuple _HOCL_tupleAtomIterator98 = (IteratorForTuple)permutation.getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution71 = (IteratorForSolution)_HOCL_tupleAtomIterator98.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator99 = (IteratorForTuple)_HOCL_iteratorSolution71.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution72 = (IteratorForSolution)_HOCL_tupleAtomIterator99.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator100 = (IteratorForTuple)_HOCL_iteratorSolution72.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution73 = (IteratorForSolution)_HOCL_tupleAtomIterator100.getAtomIterator(1);
		Molecule w_res = _HOCL_iteratorSolution73.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator101 = (IteratorForTuple)permutation.getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution74 = (IteratorForSolution)_HOCL_tupleAtomIterator101.getAtomIterator(1);
		Molecule w_i = _HOCL_iteratorSolution74.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator102 = (IteratorForTuple)permutation.getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution75 = (IteratorForSolution)_HOCL_tupleAtomIterator102.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator103 = (IteratorForTuple)_HOCL_iteratorSolution75.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution76 = (IteratorForSolution)_HOCL_tupleAtomIterator103.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator104 = (IteratorForTuple)_HOCL_iteratorSolution76.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution77 = (IteratorForSolution)_HOCL_tupleAtomIterator104.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator105 = (IteratorForTuple)_HOCL_iteratorSolution77.getSubPermutation().getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution78 = (IteratorForSolution)_HOCL_tupleAtomIterator105.getAtomIterator(1);
		Integer exit = (Integer)((IteratorForExternal)_HOCL_iteratorSolution78.getSubPermutation().getAtomIterator(0)).getObject();
		IteratorForTuple _HOCL_tupleAtomIterator106 = (IteratorForTuple)permutation.getAtomIterator(0);
		String t_j = (String)((IteratorForExternal)_HOCL_tupleAtomIterator106.getAtomIterator(0)).getObject();
		IteratorForTuple _HOCL_tupleAtomIterator107 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution79 = (IteratorForSolution)_HOCL_tupleAtomIterator107.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator108 = (IteratorForTuple)_HOCL_iteratorSolution79.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution80 = (IteratorForSolution)_HOCL_tupleAtomIterator108.getAtomIterator(1);
		Molecule w_src = _HOCL_iteratorSolution80.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator109 = (IteratorForTuple)permutation.getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution81 = (IteratorForSolution)_HOCL_tupleAtomIterator109.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator110 = (IteratorForTuple)_HOCL_iteratorSolution81.getSubPermutation().getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution82 = (IteratorForSolution)_HOCL_tupleAtomIterator110.getAtomIterator(1);
		Molecule w_dst = _HOCL_iteratorSolution82.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator111 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution83 = (IteratorForSolution)_HOCL_tupleAtomIterator111.getAtomIterator(1);
		Molecule w_j = _HOCL_iteratorSolution83.getSubPermutation().getMatchedMolecule(0);
		// new Molecule  
		Molecule mol21 = new Molecule();
		Tuple tuple19 = new Tuple(2);
		tuple19.set(0, ExternalObject.getHOCL_TypeTranslation(t_i));
		// new solution + 0 
		Solution solution23 = new Solution(); 
		{
			// new Molecule  
			Molecule mol22 = new Molecule();
			Tuple tuple20 = new Tuple(2);
			tuple20.set(0, ExternalObject.getHOCL_TypeTranslation("RES"));
			// new solution + 1 
			Solution solution21 = new Solution(); 
			{
				// new Molecule  
				Molecule mol23 = new Molecule();
				Tuple tuple21 = new Tuple(2);
				tuple21.set(0, ExternalObject.getHOCL_TypeTranslation("TRANSFER"));
				// new solution + 2 
				Solution solution20 = new Solution(); 
				{
					// new Molecule  
					Molecule mol24 = new Molecule();
					Tuple tuple22 = new Tuple(2);
					tuple22.set(0, ExternalObject.getHOCL_TypeTranslation("OUT"));
					// new solution + 3 
					Solution solution18 = new Solution(); 
					{
						// new Molecule  
						Molecule mol25 = new Molecule();
						{ 
						Atom atomReference = ExternalObject.getHOCL_TypeTranslation(out);
						if (atomReference instanceof ExternalObject) {
							ExternalObject auxObject = (ExternalObject) atomReference;
							mol25.add(auxObject);
							string = auxObject.getObject().getClass().toString().split("\\.");
							this.addType(string[string.length-1]);
						} else {
							mol25.add(atomReference);
						if(atomReference instanceof Tuple) 
							this.addType("Tuple");
						}
						} 
						
						solution18.addMolecule(mol25);
					}
					tuple22.set(1, solution18);
					tuple = tuple22;
					mol24.add(tuple);
					this.addType("Tuple");
					
					
					Tuple tuple23 = new Tuple(2);
					tuple23.set(0, ExternalObject.getHOCL_TypeTranslation("EXIT"));
					// new solution + 3 
					Solution solution19 = new Solution(); 
					{
						// new Molecule  
						Molecule mol26 = new Molecule();
						{ 
						Atom atomReference = ExternalObject.getHOCL_TypeTranslation(exit);
						if (atomReference instanceof ExternalObject) {
							ExternalObject auxObject = (ExternalObject) atomReference;
							mol26.add(auxObject);
							string = auxObject.getObject().getClass().toString().split("\\.");
							this.addType(string[string.length-1]);
						} else {
							mol26.add(atomReference);
						if(atomReference instanceof Tuple) 
							this.addType("Tuple");
						}
						} 
						
						solution19.addMolecule(mol26);
					}
					tuple23.set(1, solution19);
					tuple = tuple23;
					mol24.add(tuple);
					this.addType("Tuple");
					
					
					mol24.add(w_res);
					solution20.addMolecule(mol24);
				}
				tuple21.set(1, solution20);
				tuple = tuple21;
				mol23.add(tuple);
				this.addType("Tuple");
				
				
				solution21.addMolecule(mol23);
			}
			tuple20.set(1, solution21);
			tuple = tuple20;
			mol22.add(tuple);
			this.addType("Tuple");
			
			
			Tuple tuple24 = new Tuple(2);
			tuple24.set(0, ExternalObject.getHOCL_TypeTranslation("DST_CONTROL"));
			// new solution + 1 
			Solution solution22 = new Solution(); 
			{
				// new Molecule  
				Molecule mol27 = new Molecule();
				mol27.add(w_dst);
				solution22.addMolecule(mol27);
			}
			tuple24.set(1, solution22);
			tuple = tuple24;
			mol22.add(tuple);
			this.addType("Tuple");
			
			
			mol22.add(w_i);
			solution23.addMolecule(mol22);
		}
		tuple19.set(1, solution23);
		tuple = tuple19;
		mol21.add(tuple);
		this.addType("Tuple");
		
		
		Tuple tuple25 = new Tuple(2);
		tuple25.set(0, ExternalObject.getHOCL_TypeTranslation(t_j));
		// new solution + 0 
		Solution solution25 = new Solution(); 
		{
			// new Molecule  
			Molecule mol28 = new Molecule();
			Tuple tuple26 = new Tuple(2);
			tuple26.set(0, ExternalObject.getHOCL_TypeTranslation("SRC_CONTROL"));
			// new solution + 1 
			Solution solution24 = new Solution(); 
			{
				// new Molecule  
				Molecule mol29 = new Molecule();
				mol29.add(w_src);
				solution24.addMolecule(mol29);
			}
			tuple26.set(1, solution24);
			tuple = tuple26;
			mol28.add(tuple);
			this.addType("Tuple");
			
			
			mol28.add(w_j);
			solution25.addMolecule(mol28);
		}
		tuple25.set(1, solution25);
		tuple = tuple25;
		mol21.add(tuple);
		this.addType("Tuple");
		
		
		return mol21;
	}

} // end of class Gw_pass_control
