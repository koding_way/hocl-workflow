/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.rules.centralised;
import fr.inria.hocl.core.hocli.*;

import java.io.*;

public class Wau_update_src extends ReactionRule implements Serializable {

	/** Faulty service.*/
	private String faulty_;
	
	/** alternative service.*/
    private String alternative_;

    public Wau_update_src(String faulty, String alternative){
		super("wa_update_src", Shot.ONE_SHOT);
		
		faulty_ = faulty;
		alternative_ = alternative;
		
		setTrope(Trope.OPTIMIZER);
			AtomIterator[] _HOCL_atomIteratorArray14;
		_HOCL_atomIteratorArray14 = new AtomIterator[1];
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple11;
			_HOCL_atomIteratorArrayTuple11 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal9 extends IteratorForExternal {
					public AtomIterator__HOCL_literal9(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator34 = (IteratorForTuple)permutation.getAtomIterator(0);
							String _HOCL_literal9 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator34.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal9.equals("SRC");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal9
				_HOCL_atomIteratorArrayTuple11[0] = new AtomIterator__HOCL_literal9();
			}
			{
				class IteratorSolution23 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray14;
						_HOCL_atomIteratorArray14 = new AtomIterator[1];
						{
							class AtomIterator__HOCL_literal10 extends IteratorForExternal {
								public AtomIterator__HOCL_literal10(){
									access = Access.REWRITE;
								}
								@Override
								public boolean conditionSatisfied() {
									Atom atom;
									boolean satisfied;
									atom = iterator.getElement();
									satisfied = false;
									if (atom instanceof ExternalObject
									  && ((ExternalObject)atom).getObject() instanceof String) {
										
										IteratorForTuple _HOCL_tupleAtomIterator35 = (IteratorForTuple)permutation.getAtomIterator(0);
										IteratorForSolution _HOCL_iteratorSolution22 = (IteratorForSolution)_HOCL_tupleAtomIterator35.getAtomIterator(1);
										String _HOCL_literal10 = (String)((IteratorForExternal)_HOCL_iteratorSolution22.getSubPermutation().getAtomIterator(0)).getObject();
										satisfied = _HOCL_literal10.equals(faulty_);
									}
									return satisfied;
								}
							
							} // end of class AtomIterator__HOCL_literal10
							_HOCL_atomIteratorArray14[0] = new AtomIterator__HOCL_literal10();
						}
						MoleculeIterator[] _HOCL_moleculeIteratorArray14 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray14[0] = new MoleculeIterator(1); // w_src
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray14, _HOCL_moleculeIteratorArray14);
						return perm;
					}
				
				} // class IteratorSolution23
				_HOCL_atomIteratorArrayTuple11[1] = new IteratorSolution23();
			}
			_HOCL_atomIteratorArray14[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple11, Wau_update_src.this);
		}
		MoleculeIterator[] _HOCL_moleculeIteratorArray15 = new MoleculeIterator[0];
		permutation = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray14, _HOCL_moleculeIteratorArray15);
	}
	public Wau_update_src clone() {
		 return new Wau_update_src(faulty_, alternative_);
	}

	public void addType(String s){}

	// compute result of the rule wa_update_src
	protected Molecule computeResult(){
		ExternalObject object;
		ReactionRule rule;
		String[] string;
		Tuple tuple;
		HoclList list;		
		IteratorForTuple _HOCL_tupleAtomIterator36 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution23 = (IteratorForSolution)_HOCL_tupleAtomIterator36.getAtomIterator(1);
		Molecule w_src = _HOCL_iteratorSolution23.getSubPermutation().getMatchedMolecule(0);
		// new Molecule  
		Molecule mol14 = new Molecule();
		Tuple tuple11 = new Tuple(2);
		tuple11.set(0, ExternalObject.getHOCL_TypeTranslation("SRC"));
		// new solution + 0 
		Solution solution11 = new Solution(); 
		{
			// new Molecule  
			Molecule mol15 = new Molecule();
			{ 
			Atom atomReference = ExternalObject.getHOCL_TypeTranslation(alternative_);
			if (atomReference instanceof ExternalObject) {
				ExternalObject auxObject = (ExternalObject) atomReference;
				mol15.add(auxObject);
				string = auxObject.getObject().getClass().toString().split("\\.");
				this.addType(string[string.length-1]);
			} else {
				mol15.add(atomReference);
			if(atomReference instanceof Tuple) 
				this.addType("Tuple");
			}
			} 
			
			mol15.add(w_src);
			solution11.addMolecule(mol15);
		}
		tuple11.set(1, solution11);
		tuple = tuple11;
		mol14.add(tuple);
		this.addType("Tuple");
		
		
		return mol14;
	}

} // end of class Wa_update_src
