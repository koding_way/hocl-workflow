/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.rules.centralised;
import fr.inria.hocl.core.hocli.*;

import java.io.*;

public class Wau_update_dst extends ReactionRule implements Serializable {

    /** Faulty service.*/
    private String faulty_;
    
    /** alternative service.*/
    private String alternative_;
    
	public Wau_update_dst(String faulty, String alternative){
		super("wa_update_dst", Shot.ONE_SHOT);
		
		faulty_ = faulty;
        alternative_ = alternative;
		
		setTrope(Trope.OPTIMIZER);
			AtomIterator[] _HOCL_atomIteratorArray16;
		_HOCL_atomIteratorArray16 = new AtomIterator[1];
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple12;
			_HOCL_atomIteratorArrayTuple12 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal11 extends IteratorForExternal {
					public AtomIterator__HOCL_literal11(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator37 = (IteratorForTuple)permutation.getAtomIterator(0);
							String _HOCL_literal11 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator37.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal11.equals("DST");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal11
				_HOCL_atomIteratorArrayTuple12[0] = new AtomIterator__HOCL_literal11();
			}
			{
				class IteratorSolution25 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray16;
						_HOCL_atomIteratorArray16 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray16 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray16[0] = new MoleculeIterator(1); // w_dst
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray16, _HOCL_moleculeIteratorArray16);
						return perm;
					}
				
				} // class IteratorSolution25
				_HOCL_atomIteratorArrayTuple12[1] = new IteratorSolution25();
			}
			_HOCL_atomIteratorArray16[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple12, Wau_update_dst.this);
		}
		MoleculeIterator[] _HOCL_moleculeIteratorArray17 = new MoleculeIterator[0];
		permutation = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray16, _HOCL_moleculeIteratorArray17);
	}
	public Wau_update_dst clone() {
		 return new Wau_update_dst(faulty_, alternative_);
	}

	public void addType(String s){}

	// compute result of the rule wa_update_dst
	protected Molecule computeResult(){
		ExternalObject object;
		ReactionRule rule;
		String[] string;
		Tuple tuple;
		HoclList list;		
		IteratorForTuple _HOCL_tupleAtomIterator38 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution24 = (IteratorForSolution)_HOCL_tupleAtomIterator38.getAtomIterator(1);
		Molecule w_dst = _HOCL_iteratorSolution24.getSubPermutation().getMatchedMolecule(0);
		// new Molecule  
		Molecule mol16 = new Molecule();
		Tuple tuple12 = new Tuple(2);
		tuple12.set(0, ExternalObject.getHOCL_TypeTranslation("DST"));
		// new solution + 0 
		Solution solution12 = new Solution(); 
		{
			// new Molecule  
			Molecule mol17 = new Molecule();
			{ 
			Atom atomReference = ExternalObject.getHOCL_TypeTranslation(alternative_);
			if (atomReference instanceof ExternalObject) {
				ExternalObject auxObject = (ExternalObject) atomReference;
				mol17.add(auxObject);
				string = auxObject.getObject().getClass().toString().split("\\.");
				this.addType(string[string.length-1]);
			} else {
				mol17.add(atomReference);
			if(atomReference instanceof Tuple) 
				this.addType("Tuple");
			}
			} 
			
			mol17.add(w_dst);
			solution12.addMolecule(mol17);
		}
		tuple12.set(1, solution12);
		tuple = tuple12;
		mol16.add(tuple);
		this.addType("Tuple");
		
		
		return mol16;
	}

} // end of class Wa_update_dst
