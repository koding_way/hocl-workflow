#!/usr/bin/env bash
#
# This file is part of hocl-workflow.
#
# hocl-workflow is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# hocl-workflow is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
#


# generates the rule classes
# set the different path accordingly to your configuration


HOCL_PATH=$HOME/HOCL/hocl_git/target/
COMPILE_PATH=$HOME/HOCL/hocl_git/scripts/hoclcompile

HOCL_PATH=$HOCL_PATH $COMPILE_PATH
rm -rf bin
rm Run* run*
rm *_gen.java 

