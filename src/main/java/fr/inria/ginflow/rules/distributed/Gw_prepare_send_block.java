/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.rules.distributed;
import fr.inria.ginflow.invoker.Invoker;
import fr.inria.ginflow.distributed.TransferMolecule;
import fr.inria.hocl.core.hocli.*;
import java.io.*;

public class Gw_prepare_send_block extends ReactionRule implements Serializable {

	
	public Gw_prepare_send_block(){
		super("gw_prepare_send_block", Shot.N_SHOT);
		setTrope(Trope.EXPANSER);
			AtomIterator[] _HOCL_atomIteratorArray15;
		_HOCL_atomIteratorArray15 = new AtomIterator[1];
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple12;
			_HOCL_atomIteratorArrayTuple12 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal12 extends IteratorForExternal {
					public AtomIterator__HOCL_literal12(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator26 = (IteratorForTuple)permutation.getAtomIterator(0);
							String _HOCL_literal12 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator26.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal12.equals("RESULT");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal12
				_HOCL_atomIteratorArrayTuple12[0] = new AtomIterator__HOCL_literal12();
			}
			{
				class IteratorSolution29 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray15;
						_HOCL_atomIteratorArray15 = new AtomIterator[1];
						{
							AtomIterator[] _HOCL_atomIteratorArrayTuple13;
							_HOCL_atomIteratorArrayTuple13 = new AtomIterator[2];
							{
								class AtomIterator__HOCL_literal13 extends IteratorForExternal {
									public AtomIterator__HOCL_literal13(){
										access = Access.REWRITE;
									}
									@Override
									public boolean conditionSatisfied() {
										Atom atom;
										boolean satisfied;
										atom = iterator.getElement();
										satisfied = false;
										if (atom instanceof ExternalObject
										  && ((ExternalObject)atom).getObject() instanceof String) {
											
											IteratorForTuple _HOCL_tupleAtomIterator27 = (IteratorForTuple)permutation.getAtomIterator(0);
											IteratorForSolution _HOCL_iteratorSolution14 = (IteratorForSolution)_HOCL_tupleAtomIterator27.getAtomIterator(1);
											IteratorForTuple _HOCL_tupleAtomIterator28 = (IteratorForTuple)_HOCL_iteratorSolution14.getSubPermutation().getAtomIterator(0);
											String _HOCL_literal13 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator28.getAtomIterator(0)).getObject();
											satisfied = _HOCL_literal13.equals("TRANSFER");
										}
										return satisfied;
									}
								
								} // end of class AtomIterator__HOCL_literal13
								_HOCL_atomIteratorArrayTuple13[0] = new AtomIterator__HOCL_literal13();
							}
							{
								class IteratorSolution27 extends IteratorForSolution {
									protected Permutation makeSubPermutation(){
										Permutation perm;
										AtomIterator[] _HOCL_atomIteratorArray15;
										_HOCL_atomIteratorArray15 = new AtomIterator[1];
										{
											AtomIterator[] _HOCL_atomIteratorArrayTuple14;
											_HOCL_atomIteratorArrayTuple14 = new AtomIterator[2];
											{
												class AtomIterator__HOCL_literal14 extends IteratorForExternal {
													public AtomIterator__HOCL_literal14(){
														access = Access.REWRITE;
													}
													@Override
													public boolean conditionSatisfied() {
														Atom atom;
														boolean satisfied;
														atom = iterator.getElement();
														satisfied = false;
														if (atom instanceof ExternalObject
														  && ((ExternalObject)atom).getObject() instanceof String) {
															
															IteratorForTuple _HOCL_tupleAtomIterator29 = (IteratorForTuple)permutation.getAtomIterator(0);
															IteratorForSolution _HOCL_iteratorSolution15 = (IteratorForSolution)_HOCL_tupleAtomIterator29.getAtomIterator(1);
															IteratorForTuple _HOCL_tupleAtomIterator30 = (IteratorForTuple)_HOCL_iteratorSolution15.getSubPermutation().getAtomIterator(0);
															IteratorForSolution _HOCL_iteratorSolution16 = (IteratorForSolution)_HOCL_tupleAtomIterator30.getAtomIterator(1);
															IteratorForTuple _HOCL_tupleAtomIterator31 = (IteratorForTuple)_HOCL_iteratorSolution16.getSubPermutation().getAtomIterator(0);
															String _HOCL_literal14 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator31.getAtomIterator(0)).getObject();
															satisfied = _HOCL_literal14.equals("EXIT");
														}
														return satisfied;
													}
												
												} // end of class AtomIterator__HOCL_literal14
												_HOCL_atomIteratorArrayTuple14[0] = new AtomIterator__HOCL_literal14();
											}
											{
												class IteratorSolution25 extends IteratorForSolution {
													protected Permutation makeSubPermutation(){
														Permutation perm;
														AtomIterator[] _HOCL_atomIteratorArray15;
														_HOCL_atomIteratorArray15 = new AtomIterator[1];
														{
															class AtomIterator_e extends IteratorForExternal {
																public AtomIterator_e(){
																	access = Access.REWRITE;
																}
																@Override
																public boolean conditionSatisfied() {
																	Atom atom;
																	boolean satisfied;
																	atom = iterator.getElement();
																	satisfied = false;
																	if (atom instanceof ExternalObject
																	  && ((ExternalObject)atom).getObject() instanceof Integer) {
																		
																		IteratorForTuple _HOCL_tupleAtomIterator32 = (IteratorForTuple)permutation.getAtomIterator(0);
																		IteratorForSolution _HOCL_iteratorSolution17 = (IteratorForSolution)_HOCL_tupleAtomIterator32.getAtomIterator(1);
																		IteratorForTuple _HOCL_tupleAtomIterator33 = (IteratorForTuple)_HOCL_iteratorSolution17.getSubPermutation().getAtomIterator(0);
																		IteratorForSolution _HOCL_iteratorSolution18 = (IteratorForSolution)_HOCL_tupleAtomIterator33.getAtomIterator(1);
																		IteratorForTuple _HOCL_tupleAtomIterator34 = (IteratorForTuple)_HOCL_iteratorSolution18.getSubPermutation().getAtomIterator(0);
																		IteratorForSolution _HOCL_iteratorSolution19 = (IteratorForSolution)_HOCL_tupleAtomIterator34.getAtomIterator(1);
																		Integer e = (Integer)((IteratorForExternal)_HOCL_iteratorSolution19.getSubPermutation().getAtomIterator(0)).getObject();
																		satisfied = (e == 0);
																	}
																	return satisfied;
																}
															
															} // end of class AtomIterator_e
															_HOCL_atomIteratorArray15[0] = new AtomIterator_e();
														}
														MoleculeIterator[] _HOCL_moleculeIteratorArray15 = new MoleculeIterator[0];
														perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray15, _HOCL_moleculeIteratorArray15);
														return perm;
													}
												
												} // class IteratorSolution25
												_HOCL_atomIteratorArrayTuple14[1] = new IteratorSolution25();
											}
											_HOCL_atomIteratorArray15[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple14, Gw_prepare_send_block.this);
										}
										MoleculeIterator[] _HOCL_moleculeIteratorArray16 = new MoleculeIterator[1];
										_HOCL_moleculeIteratorArray16[0] = new MoleculeIterator(1); // w_transfer
										
										perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray15, _HOCL_moleculeIteratorArray16);
										return perm;
									}
								
								} // class IteratorSolution27
								_HOCL_atomIteratorArrayTuple13[1] = new IteratorSolution27();
							}
							_HOCL_atomIteratorArray15[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple13, Gw_prepare_send_block.this);
						}
						MoleculeIterator[] _HOCL_moleculeIteratorArray17 = new MoleculeIterator[0];
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray15, _HOCL_moleculeIteratorArray17);
						return perm;
					}
				
				} // class IteratorSolution29
				_HOCL_atomIteratorArrayTuple12[1] = new IteratorSolution29();
			}
			_HOCL_atomIteratorArray15[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple12, Gw_prepare_send_block.this);
		}
		MoleculeIterator[] _HOCL_moleculeIteratorArray18 = new MoleculeIterator[0];
		permutation = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray15, _HOCL_moleculeIteratorArray18);
	}
	public Gw_prepare_send_block clone() {
		 return new Gw_prepare_send_block();
	}

	public void addType(String s){}

	// compute result of the rule gw_prepare_send_block
	protected Molecule computeResult(){
		ExternalObject object;
		ReactionRule rule;
		String[] string;
		Tuple tuple;
		HoclList list;		
		IteratorForTuple _HOCL_tupleAtomIterator35 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution20 = (IteratorForSolution)_HOCL_tupleAtomIterator35.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator36 = (IteratorForTuple)_HOCL_iteratorSolution20.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution21 = (IteratorForSolution)_HOCL_tupleAtomIterator36.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator37 = (IteratorForTuple)_HOCL_iteratorSolution21.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution22 = (IteratorForSolution)_HOCL_tupleAtomIterator37.getAtomIterator(1);
		Integer e = (Integer)((IteratorForExternal)_HOCL_iteratorSolution22.getSubPermutation().getAtomIterator(0)).getObject();
		IteratorForTuple _HOCL_tupleAtomIterator38 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution23 = (IteratorForSolution)_HOCL_tupleAtomIterator38.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator39 = (IteratorForTuple)_HOCL_iteratorSolution23.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution24 = (IteratorForSolution)_HOCL_tupleAtomIterator39.getAtomIterator(1);
		Molecule w_transfer = _HOCL_iteratorSolution24.getSubPermutation().getMatchedMolecule(0);
		// new Molecule  
		Molecule mol14 = new Molecule();
		Tuple tuple13 = new Tuple(2);
		tuple13.set(0, ExternalObject.getHOCL_TypeTranslation("RES"));
		// new solution + 0 
		Solution solution13 = new Solution(); 
		{
			// new Molecule  
			Molecule mol15 = new Molecule();
			Tuple tuple14 = new Tuple(2);
			tuple14.set(0, ExternalObject.getHOCL_TypeTranslation("TRANSFER"));
			// new solution + 1 
			Solution solution12 = new Solution(); 
			{
				// new Molecule  
				Molecule mol16 = new Molecule();
				Tuple tuple15 = new Tuple(2);
				tuple15.set(0, ExternalObject.getHOCL_TypeTranslation("EXIT"));
				// new solution + 2 
				Solution solution11 = new Solution(); 
				{
					// new Molecule  
					Molecule mol17 = new Molecule();
					{ 
					Atom atomReference = ExternalObject.getHOCL_TypeTranslation(e);
					if (atomReference instanceof ExternalObject) {
						ExternalObject auxObject = (ExternalObject) atomReference;
						mol17.add(auxObject);
						string = auxObject.getObject().getClass().toString().split("\\.");
						this.addType(string[string.length-1]);
					} else {
						mol17.add(atomReference);
					if(atomReference instanceof Tuple) 
						this.addType("Tuple");
					}
					} 
					
					solution11.addMolecule(mol17);
				}
				tuple15.set(1, solution11);
				tuple = tuple15;
				mol16.add(tuple);
				this.addType("Tuple");
				
				
				mol16.add(w_transfer);
				solution12.addMolecule(mol16);
			}
			tuple14.set(1, solution12);
			tuple = tuple14;
			mol15.add(tuple);
			this.addType("Tuple");
			
			
			solution13.addMolecule(mol15);
		}
		tuple13.set(1, solution13);
		tuple = tuple13;
		mol14.add(tuple);
		this.addType("Tuple");
		
		
		Tuple tuple16 = new Tuple(2);
		tuple16.set(0, ExternalObject.getHOCL_TypeTranslation("SEND"));
		// new solution + 0 
		Solution solution16 = new Solution(); 
		{
			// new Molecule  
			Molecule mol18 = new Molecule();
			Tuple tuple17 = new Tuple(2);
			tuple17.set(0, ExternalObject.getHOCL_TypeTranslation("TRANSFER"));
			// new solution + 1 
			Solution solution15 = new Solution(); 
			{
				// new Molecule  
				Molecule mol19 = new Molecule();
				Tuple tuple18 = new Tuple(2);
				tuple18.set(0, ExternalObject.getHOCL_TypeTranslation("EXIT"));
				// new solution + 2 
				Solution solution14 = new Solution(); 
				{
					// new Molecule  
					Molecule mol20 = new Molecule();
					{ 
					Atom atomReference = ExternalObject.getHOCL_TypeTranslation(e);
					if (atomReference instanceof ExternalObject) {
						ExternalObject auxObject = (ExternalObject) atomReference;
						mol20.add(auxObject);
						string = auxObject.getObject().getClass().toString().split("\\.");
						this.addType(string[string.length-1]);
					} else {
						mol20.add(atomReference);
					if(atomReference instanceof Tuple) 
						this.addType("Tuple");
					}
					} 
					
					solution14.addMolecule(mol20);
				}
				tuple18.set(1, solution14);
				tuple = tuple18;
				mol19.add(tuple);
				this.addType("Tuple");
				
				
				mol19.add(w_transfer);
				solution15.addMolecule(mol19);
			}
			tuple17.set(1, solution15);
			tuple = tuple17;
			mol18.add(tuple);
			this.addType("Tuple");
			
			
			solution16.addMolecule(mol18);
		}
		tuple16.set(1, solution16);
		tuple = tuple16;
		mol14.add(tuple);
		this.addType("Tuple");
		
		
		return mol14;
	}

} // end of class Gw_prepare_send_block
