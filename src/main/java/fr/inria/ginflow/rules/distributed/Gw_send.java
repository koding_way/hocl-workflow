/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.rules.distributed;
import fr.inria.ginflow.invoker.Invoker;
import fr.inria.ginflow.distributed.TransferMolecule;
import fr.inria.hocl.core.hocli.*;
import java.io.*;

public class Gw_send extends ReactionRule implements Serializable {

	
	public Gw_send(){
		super("gw_send", Shot.N_SHOT);
		setTrope(Trope.EXPANSER);
			AtomIterator[] _HOCL_atomIteratorArray23;
		_HOCL_atomIteratorArray23 = new AtomIterator[2];
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple18;
			_HOCL_atomIteratorArrayTuple18 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal18 extends IteratorForExternal {
					public AtomIterator__HOCL_literal18(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator51 = (IteratorForTuple)permutation.getAtomIterator(0);
							String _HOCL_literal18 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator51.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal18.equals("DST");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal18
				_HOCL_atomIteratorArrayTuple18[0] = new AtomIterator__HOCL_literal18();
			}
			{
				class IteratorSolution37 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray23;
						_HOCL_atomIteratorArray23 = new AtomIterator[1];
						{
							class AtomIterator_d extends IteratorForExternal {
								public AtomIterator_d(){
									access = Access.REWRITE;
								}
								@Override
								public boolean conditionSatisfied() {
									Atom atom;
									boolean satisfied;
									atom = iterator.getElement();
									satisfied = false;
									if (atom instanceof ExternalObject
									  && ((ExternalObject)atom).getObject() instanceof String) {
										satisfied = true;
									}
									return satisfied;
								}
							
							} // end of class AtomIterator_d
							_HOCL_atomIteratorArray23[0] = new AtomIterator_d();
						}
						MoleculeIterator[] _HOCL_moleculeIteratorArray23 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray23[0] = new MoleculeIterator(1); // w_dst
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray23, _HOCL_moleculeIteratorArray23);
						return perm;
					}
				
				} // class IteratorSolution37
				_HOCL_atomIteratorArrayTuple18[1] = new IteratorSolution37();
			}
			_HOCL_atomIteratorArray23[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple18, Gw_send.this);
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple19;
			_HOCL_atomIteratorArrayTuple19 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal19 extends IteratorForExternal {
					public AtomIterator__HOCL_literal19(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator52 = (IteratorForTuple)permutation.getAtomIterator(1);
							String _HOCL_literal19 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator52.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal19.equals("SEND");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal19
				_HOCL_atomIteratorArrayTuple19[0] = new AtomIterator__HOCL_literal19();
			}
			{
				class IteratorSolution39 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray24;
						_HOCL_atomIteratorArray24 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray24 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray24[0] = new MoleculeIterator(1); // w_res
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray24, _HOCL_moleculeIteratorArray24);
						return perm;
					}
				
				} // class IteratorSolution39
				_HOCL_atomIteratorArrayTuple19[1] = new IteratorSolution39();
			}
			_HOCL_atomIteratorArray23[1] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple19, Gw_send.this);
		}
		MoleculeIterator[] _HOCL_moleculeIteratorArray25 = new MoleculeIterator[0];
		permutation = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray23, _HOCL_moleculeIteratorArray25);
	}
	public Gw_send clone() {
		 return new Gw_send();
	}

	public void addType(String s){}

	// compute result of the rule gw_send
	protected Molecule computeResult(){
		ExternalObject object;
		ReactionRule rule;
		String[] string;
		Tuple tuple;
		HoclList list;		
		IteratorForTuple _HOCL_tupleAtomIterator53 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution33 = (IteratorForSolution)_HOCL_tupleAtomIterator53.getAtomIterator(1);
		String d = (String)((IteratorForExternal)_HOCL_iteratorSolution33.getSubPermutation().getAtomIterator(0)).getObject();
		IteratorForTuple _HOCL_tupleAtomIterator54 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution34 = (IteratorForSolution)_HOCL_tupleAtomIterator54.getAtomIterator(1);
		Molecule w_dst = _HOCL_iteratorSolution34.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator55 = (IteratorForTuple)permutation.getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution35 = (IteratorForSolution)_HOCL_tupleAtomIterator55.getAtomIterator(1);
		Molecule w_res = _HOCL_iteratorSolution35.getSubPermutation().getMatchedMolecule(0);
		// new Molecule  
		Molecule mol28 = new Molecule();
		Tuple tuple25 = new Tuple(2);
		tuple25.set(0, ExternalObject.getHOCL_TypeTranslation("TRF"));
		tuple25.set(1, ExternalObject.getHOCL_TypeTranslation(TransferMolecule.put(d, w_res)));
		tuple = tuple25;
		mol28.add(tuple);
		this.addType("Tuple");
		
		
		Tuple tuple26 = new Tuple(2);
		tuple26.set(0, ExternalObject.getHOCL_TypeTranslation("DST"));
		// new solution + 0 
		Solution solution23 = new Solution(); 
		{
			// new Molecule  
			Molecule mol29 = new Molecule();
			mol29.add(w_dst);
			solution23.addMolecule(mol29);
		}
		tuple26.set(1, solution23);
		tuple = tuple26;
		mol28.add(tuple);
		this.addType("Tuple");
		
		
		Tuple tuple27 = new Tuple(2);
		tuple27.set(0, ExternalObject.getHOCL_TypeTranslation("SEND"));
		// new solution + 0 
		Solution solution24 = new Solution(); 
		{
			// new Molecule  
			Molecule mol30 = new Molecule();
			mol30.add(w_res);
			solution24.addMolecule(mol30);
		}
		tuple27.set(1, solution24);
		tuple = tuple27;
		mol28.add(tuple);
		this.addType("Tuple");
		
		
		return mol28;
	}

} // end of class Gw_send
