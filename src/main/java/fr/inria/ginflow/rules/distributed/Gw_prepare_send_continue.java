/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.rules.distributed;
import fr.inria.ginflow.invoker.Invoker;
import fr.inria.ginflow.distributed.TransferMolecule;
import fr.inria.hocl.core.hocli.*;
import java.io.*;

public class Gw_prepare_send_continue extends ReactionRule implements Serializable {

	
	public Gw_prepare_send_continue(){
		super("gw_prepare_send_continue", Shot.N_SHOT);
		setTrope(Trope.EXPANSER);
			AtomIterator[] _HOCL_atomIteratorArray19;
		_HOCL_atomIteratorArray19 = new AtomIterator[1];
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple15;
			_HOCL_atomIteratorArrayTuple15 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal15 extends IteratorForExternal {
					public AtomIterator__HOCL_literal15(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator40 = (IteratorForTuple)permutation.getAtomIterator(0);
							String _HOCL_literal15 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator40.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal15.equals("RESULT");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal15
				_HOCL_atomIteratorArrayTuple15[0] = new AtomIterator__HOCL_literal15();
			}
			{
				class IteratorSolution35 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray19;
						_HOCL_atomIteratorArray19 = new AtomIterator[1];
						{
							AtomIterator[] _HOCL_atomIteratorArrayTuple16;
							_HOCL_atomIteratorArrayTuple16 = new AtomIterator[2];
							{
								class AtomIterator__HOCL_literal16 extends IteratorForExternal {
									public AtomIterator__HOCL_literal16(){
										access = Access.REWRITE;
									}
									@Override
									public boolean conditionSatisfied() {
										Atom atom;
										boolean satisfied;
										atom = iterator.getElement();
										satisfied = false;
										if (atom instanceof ExternalObject
										  && ((ExternalObject)atom).getObject() instanceof String) {
											
											IteratorForTuple _HOCL_tupleAtomIterator41 = (IteratorForTuple)permutation.getAtomIterator(0);
											IteratorForSolution _HOCL_iteratorSolution25 = (IteratorForSolution)_HOCL_tupleAtomIterator41.getAtomIterator(1);
											IteratorForTuple _HOCL_tupleAtomIterator42 = (IteratorForTuple)_HOCL_iteratorSolution25.getSubPermutation().getAtomIterator(0);
											String _HOCL_literal16 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator42.getAtomIterator(0)).getObject();
											satisfied = _HOCL_literal16.equals("TRANSFER");
										}
										return satisfied;
									}
								
								} // end of class AtomIterator__HOCL_literal16
								_HOCL_atomIteratorArrayTuple16[0] = new AtomIterator__HOCL_literal16();
							}
							{
								class IteratorSolution33 extends IteratorForSolution {
									protected Permutation makeSubPermutation(){
										Permutation perm;
										AtomIterator[] _HOCL_atomIteratorArray19;
										_HOCL_atomIteratorArray19 = new AtomIterator[1];
										{
											AtomIterator[] _HOCL_atomIteratorArrayTuple17;
											_HOCL_atomIteratorArrayTuple17 = new AtomIterator[2];
											{
												class AtomIterator__HOCL_literal17 extends IteratorForExternal {
													public AtomIterator__HOCL_literal17(){
														access = Access.REWRITE;
													}
													@Override
													public boolean conditionSatisfied() {
														Atom atom;
														boolean satisfied;
														atom = iterator.getElement();
														satisfied = false;
														if (atom instanceof ExternalObject
														  && ((ExternalObject)atom).getObject() instanceof String) {
															
															IteratorForTuple _HOCL_tupleAtomIterator43 = (IteratorForTuple)permutation.getAtomIterator(0);
															IteratorForSolution _HOCL_iteratorSolution26 = (IteratorForSolution)_HOCL_tupleAtomIterator43.getAtomIterator(1);
															IteratorForTuple _HOCL_tupleAtomIterator44 = (IteratorForTuple)_HOCL_iteratorSolution26.getSubPermutation().getAtomIterator(0);
															IteratorForSolution _HOCL_iteratorSolution27 = (IteratorForSolution)_HOCL_tupleAtomIterator44.getAtomIterator(1);
															IteratorForTuple _HOCL_tupleAtomIterator45 = (IteratorForTuple)_HOCL_iteratorSolution27.getSubPermutation().getAtomIterator(0);
															String _HOCL_literal17 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator45.getAtomIterator(0)).getObject();
															satisfied = _HOCL_literal17.equals("EXIT");
														}
														return satisfied;
													}
												
												} // end of class AtomIterator__HOCL_literal17
												_HOCL_atomIteratorArrayTuple17[0] = new AtomIterator__HOCL_literal17();
											}
											{
												class IteratorSolution31 extends IteratorForSolution {
													protected Permutation makeSubPermutation(){
														Permutation perm;
														AtomIterator[] _HOCL_atomIteratorArray19;
														_HOCL_atomIteratorArray19 = new AtomIterator[1];
														{
															class AtomIterator_e extends IteratorForExternal {
																public AtomIterator_e(){
																	access = Access.REWRITE;
																}
																@Override
																public boolean conditionSatisfied() {
																	Atom atom;
																	boolean satisfied;
																	atom = iterator.getElement();
																	satisfied = false;
																	if (atom instanceof ExternalObject
																	  && ((ExternalObject)atom).getObject() instanceof Integer) {
																		satisfied = true;
																	}
																	return satisfied;
																}
															
															} // end of class AtomIterator_e
															_HOCL_atomIteratorArray19[0] = new AtomIterator_e();
														}
														MoleculeIterator[] _HOCL_moleculeIteratorArray19 = new MoleculeIterator[0];
														perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray19, _HOCL_moleculeIteratorArray19);
														return perm;
													}
												
												} // class IteratorSolution31
												_HOCL_atomIteratorArrayTuple17[1] = new IteratorSolution31();
											}
											_HOCL_atomIteratorArray19[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple17, Gw_prepare_send_continue.this);
										}
										MoleculeIterator[] _HOCL_moleculeIteratorArray20 = new MoleculeIterator[1];
										_HOCL_moleculeIteratorArray20[0] = new MoleculeIterator(1); // w_transfer
										
										perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray19, _HOCL_moleculeIteratorArray20);
										return perm;
									}
								
								} // class IteratorSolution33
								_HOCL_atomIteratorArrayTuple16[1] = new IteratorSolution33();
							}
							_HOCL_atomIteratorArray19[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple16, Gw_prepare_send_continue.this);
						}
						MoleculeIterator[] _HOCL_moleculeIteratorArray21 = new MoleculeIterator[0];
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray19, _HOCL_moleculeIteratorArray21);
						return perm;
					}
				
				} // class IteratorSolution35
				_HOCL_atomIteratorArrayTuple15[1] = new IteratorSolution35();
			}
			_HOCL_atomIteratorArray19[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple15, Gw_prepare_send_continue.this);
		}
		MoleculeIterator[] _HOCL_moleculeIteratorArray22 = new MoleculeIterator[0];
		permutation = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray19, _HOCL_moleculeIteratorArray22);
	}
	public Gw_prepare_send_continue clone() {
		 return new Gw_prepare_send_continue();
	}

	public void addType(String s){}

	// compute result of the rule gw_prepare_send_continue
	protected Molecule computeResult(){
		ExternalObject object;
		ReactionRule rule;
		String[] string;
		Tuple tuple;
		HoclList list;		
		IteratorForTuple _HOCL_tupleAtomIterator46 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution28 = (IteratorForSolution)_HOCL_tupleAtomIterator46.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator47 = (IteratorForTuple)_HOCL_iteratorSolution28.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution29 = (IteratorForSolution)_HOCL_tupleAtomIterator47.getAtomIterator(1);
		Molecule w_transfer = _HOCL_iteratorSolution29.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator48 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution30 = (IteratorForSolution)_HOCL_tupleAtomIterator48.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator49 = (IteratorForTuple)_HOCL_iteratorSolution30.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution31 = (IteratorForSolution)_HOCL_tupleAtomIterator49.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator50 = (IteratorForTuple)_HOCL_iteratorSolution31.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution32 = (IteratorForSolution)_HOCL_tupleAtomIterator50.getAtomIterator(1);
		Integer e = (Integer)((IteratorForExternal)_HOCL_iteratorSolution32.getSubPermutation().getAtomIterator(0)).getObject();
		// new Molecule  
		Molecule mol21 = new Molecule();
		Tuple tuple19 = new Tuple(2);
		tuple19.set(0, ExternalObject.getHOCL_TypeTranslation("RES"));
		// new solution + 0 
		Solution solution19 = new Solution(); 
		{
			// new Molecule  
			Molecule mol22 = new Molecule();
			Tuple tuple20 = new Tuple(2);
			tuple20.set(0, ExternalObject.getHOCL_TypeTranslation("TRANSFER"));
			// new solution + 1 
			Solution solution18 = new Solution(); 
			{
				// new Molecule  
				Molecule mol23 = new Molecule();
				Tuple tuple21 = new Tuple(2);
				tuple21.set(0, ExternalObject.getHOCL_TypeTranslation("EXIT"));
				// new solution + 2 
				Solution solution17 = new Solution(); 
				{
					// new Molecule  
					Molecule mol24 = new Molecule();
					{ 
					Atom atomReference = ExternalObject.getHOCL_TypeTranslation(e);
					if (atomReference instanceof ExternalObject) {
						ExternalObject auxObject = (ExternalObject) atomReference;
						mol24.add(auxObject);
						string = auxObject.getObject().getClass().toString().split("\\.");
						this.addType(string[string.length-1]);
					} else {
						mol24.add(atomReference);
					if(atomReference instanceof Tuple) 
						this.addType("Tuple");
					}
					} 
					
					solution17.addMolecule(mol24);
				}
				tuple21.set(1, solution17);
				tuple = tuple21;
				mol23.add(tuple);
				this.addType("Tuple");
				
				
				mol23.add(w_transfer);
				solution18.addMolecule(mol23);
			}
			tuple20.set(1, solution18);
			tuple = tuple20;
			mol22.add(tuple);
			this.addType("Tuple");
			
			
			solution19.addMolecule(mol22);
		}
		tuple19.set(1, solution19);
		tuple = tuple19;
		mol21.add(tuple);
		this.addType("Tuple");
		
		
		Tuple tuple22 = new Tuple(2);
		tuple22.set(0, ExternalObject.getHOCL_TypeTranslation("SEND"));
		// new solution + 0 
		Solution solution22 = new Solution(); 
		{
			// new Molecule  
			Molecule mol25 = new Molecule();
			Tuple tuple23 = new Tuple(2);
			tuple23.set(0, ExternalObject.getHOCL_TypeTranslation("TRANSFER"));
			// new solution + 1 
			Solution solution21 = new Solution(); 
			{
				// new Molecule  
				Molecule mol26 = new Molecule();
				Tuple tuple24 = new Tuple(2);
				tuple24.set(0, ExternalObject.getHOCL_TypeTranslation("EXIT"));
				// new solution + 2 
				Solution solution20 = new Solution(); 
				{
					// new Molecule  
					Molecule mol27 = new Molecule();
					{ 
					Atom atomReference = ExternalObject.getHOCL_TypeTranslation(e);
					if (atomReference instanceof ExternalObject) {
						ExternalObject auxObject = (ExternalObject) atomReference;
						mol27.add(auxObject);
						string = auxObject.getObject().getClass().toString().split("\\.");
						this.addType(string[string.length-1]);
					} else {
						mol27.add(atomReference);
					if(atomReference instanceof Tuple) 
						this.addType("Tuple");
					}
					} 
					
					solution20.addMolecule(mol27);
				}
				tuple24.set(1, solution20);
				tuple = tuple24;
				mol26.add(tuple);
				this.addType("Tuple");
				
				
				mol26.add(w_transfer);
				solution21.addMolecule(mol26);
			}
			tuple23.set(1, solution21);
			tuple = tuple23;
			mol25.add(tuple);
			this.addType("Tuple");
			
			
			solution22.addMolecule(mol25);
		}
		tuple22.set(1, solution22);
		tuple = tuple22;
		mol21.add(tuple);
		this.addType("Tuple");
		
		
		return mol21;
	}

} // end of class Gw_prepare_send_continue
