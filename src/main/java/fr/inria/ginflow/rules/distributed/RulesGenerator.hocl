package fr.inria.ginflow.rules.distributed;

import fr.inria.ginflow.invoker.Invoker;
import fr.inria.ginflow.distributed.TransferMolecule;

let gw_setup =
  replace-one "SRC":<>, "SRC_CONTROL":<>, "IN":<?w_in>
  by "SRC":<>, "SRC_CONTROL":<>, "PAR":<w_in> 
in
let gw_call =
  replace-one "SRC":<>, "SRV":<service::String>, "PAR":<?w_par>, "NAME":<name::String>, "ENV":<?w_env>
  by "SRC":<>, "SRV":<service>, "RESULT":<"TRANSFER":Invoker.call(name, service, w_par, w_env)>, "NAME":<name>, "ENV":<w_env> 
in
// adapt (should be used in conjonction with block or continue)
let gw_adapt =
  replace "RESULT":<"TRANSFER":<"EXIT":<e::int>, ?w_transfer>>, "ADAPT":<?w_adapt_dst>
  by "TRF":TransferMolecule.put("ADAPT", w_adapt_dst, "ADAPT"), "RESULT":<"TRANSFER":<"EXIT":<e>, w_transfer>>	
  if (e != 0)
in
// block non exit status
let gw_prepare_send_block =
  replace "RESULT":<"TRANSFER":<"EXIT":<e::int>, ?w_transfer>>
  by "RES":<"TRANSFER":<"EXIT":<e>, w_transfer>>, "SEND":<"TRANSFER":<"EXIT":<e>, w_transfer>>
  if (e == 0)
in
// continue even if exit status is non zero
let gw_prepare_send_continue =
  replace "RESULT":<"TRANSFER":<"EXIT":<e::int>, ?w_transfer>>
  by "RES":<"TRANSFER":<"EXIT":<e>, w_transfer>>, "SEND":<"TRANSFER":<"EXIT":<e>, w_transfer>>
in
let gw_send = 
  replace "DST":<d::String, ?w_dst>, "SEND":<?w_res >
  by "TRF":TransferMolecule.put(d, w_res), "DST":<w_dst>, "SEND":<w_res>
in
let gw_send_control = 
  replace "DST_CONTROL":<d::String, ?w_dst>, "SEND":<?w_res >
  by "TRF":TransferMolecule.put(d, w_res), "DST_CONTROL":<w_dst>, "SEND":<w_res>
in
let gw_receive = 
  replace  "IN":<?w_in>, "SRC":<provenance::String, ?w_src>, "TRANSFER":<"OUT":<?w_out>, "NAME":<s::String>, "EXIT":<e::int>, ?w_transfer>
  by "SRC":<w_src>, "IN":<w_out, w_in>
  if (s.equals(provenance))
in 
let gw_receive_control =
  replace  "SRC_CONTROL":<provenance::String, ?w_src>, "TRANSFER":<"OUT":<?w_out>, "NAME":<s::String>, "EXIT":<e::int>, ?w_transfer>
  by "SRC_CONTROL":<w_src>
  if (s.equals(provenance))
in
let gw_swap_dst = 
  replace-one "ADAPT", "ADAPT_DST":<?w_adapt_dst>, "DST":<?w_dst>, "ADAPT_DST_CONTROL":<?w_adapt_dst_control>, "DST_CONTROL":<?w_dst_control>
  by "DST":<w_adapt_dst>, "DST_CONTROL":<w_adapt_dst_control>
in
// swap sources and empty IN (services may have sent there results
let gw_swap_src = 
  replace-one "ADAPT", "ADAPT_SRC":<?w_adapt_src>, "SRC":<?w_src>, "ADAPT_SRC_CONTROL":<?w_adapt_src_control>, "SRC_CONTROL":<?w_src_control>, "IN":<?w_in>
  by "SRC":<w_adapt_src>, "SRC_CONTROL":<w_adapt_src_control>, "IN":<>
in
<gw_call, gw_setup, gw_send, gw_send_control, gw_receive, gw_receive_control, gw_adapt, gw_swap_dst, gw_swap_src, gw_prepare_send_block, gw_prepare_send_continue>
    
