/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.rules.distributed;
import fr.inria.ginflow.invoker.Invoker;
import fr.inria.ginflow.distributed.TransferMolecule;
import fr.inria.hocl.core.hocli.*;
import java.io.*;

public class Gw_call extends ReactionRule implements Serializable {

	
	public Gw_call(){
		super("gw_call", Shot.ONE_SHOT);
		setTrope(Trope.OPTIMIZER);
			AtomIterator[] _HOCL_atomIteratorArray4;
		_HOCL_atomIteratorArray4 = new AtomIterator[5];
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple3;
			_HOCL_atomIteratorArrayTuple3 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal3 extends IteratorForExternal {
					public AtomIterator__HOCL_literal3(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator4 = (IteratorForTuple)permutation.getAtomIterator(0);
							String _HOCL_literal3 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator4.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal3.equals("SRC");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal3
				_HOCL_atomIteratorArrayTuple3[0] = new AtomIterator__HOCL_literal3();
			}
			{
				class IteratorSolution7 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray4;
						_HOCL_atomIteratorArray4 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray4 = new MoleculeIterator[0];
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray4, _HOCL_moleculeIteratorArray4);
						return perm;
					}
				
				} // class IteratorSolution7
				_HOCL_atomIteratorArrayTuple3[1] = new IteratorSolution7();
			}
			_HOCL_atomIteratorArray4[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple3, Gw_call.this);
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple4;
			_HOCL_atomIteratorArrayTuple4 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal4 extends IteratorForExternal {
					public AtomIterator__HOCL_literal4(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator5 = (IteratorForTuple)permutation.getAtomIterator(1);
							String _HOCL_literal4 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator5.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal4.equals("SRV");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal4
				_HOCL_atomIteratorArrayTuple4[0] = new AtomIterator__HOCL_literal4();
			}
			{
				class IteratorSolution9 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray5;
						_HOCL_atomIteratorArray5 = new AtomIterator[1];
						{
							class AtomIterator_service extends IteratorForExternal {
								public AtomIterator_service(){
									access = Access.REWRITE;
								}
								@Override
								public boolean conditionSatisfied() {
									Atom atom;
									boolean satisfied;
									atom = iterator.getElement();
									satisfied = false;
									if (atom instanceof ExternalObject
									  && ((ExternalObject)atom).getObject() instanceof String) {
										satisfied = true;
									}
									return satisfied;
								}
							
							} // end of class AtomIterator_service
							_HOCL_atomIteratorArray5[0] = new AtomIterator_service();
						}
						MoleculeIterator[] _HOCL_moleculeIteratorArray5 = new MoleculeIterator[0];
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray5, _HOCL_moleculeIteratorArray5);
						return perm;
					}
				
				} // class IteratorSolution9
				_HOCL_atomIteratorArrayTuple4[1] = new IteratorSolution9();
			}
			_HOCL_atomIteratorArray4[1] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple4, Gw_call.this);
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple5;
			_HOCL_atomIteratorArrayTuple5 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal5 extends IteratorForExternal {
					public AtomIterator__HOCL_literal5(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator6 = (IteratorForTuple)permutation.getAtomIterator(2);
							String _HOCL_literal5 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator6.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal5.equals("PAR");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal5
				_HOCL_atomIteratorArrayTuple5[0] = new AtomIterator__HOCL_literal5();
			}
			{
				class IteratorSolution11 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray6;
						_HOCL_atomIteratorArray6 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray6 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray6[0] = new MoleculeIterator(1); // w_par
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray6, _HOCL_moleculeIteratorArray6);
						return perm;
					}
				
				} // class IteratorSolution11
				_HOCL_atomIteratorArrayTuple5[1] = new IteratorSolution11();
			}
			_HOCL_atomIteratorArray4[2] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple5, Gw_call.this);
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple6;
			_HOCL_atomIteratorArrayTuple6 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal6 extends IteratorForExternal {
					public AtomIterator__HOCL_literal6(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator7 = (IteratorForTuple)permutation.getAtomIterator(3);
							String _HOCL_literal6 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator7.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal6.equals("NAME");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal6
				_HOCL_atomIteratorArrayTuple6[0] = new AtomIterator__HOCL_literal6();
			}
			{
				class IteratorSolution13 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray7;
						_HOCL_atomIteratorArray7 = new AtomIterator[1];
						{
							class AtomIterator_name extends IteratorForExternal {
								public AtomIterator_name(){
									access = Access.REWRITE;
								}
								@Override
								public boolean conditionSatisfied() {
									Atom atom;
									boolean satisfied;
									atom = iterator.getElement();
									satisfied = false;
									if (atom instanceof ExternalObject
									  && ((ExternalObject)atom).getObject() instanceof String) {
										satisfied = true;
									}
									return satisfied;
								}
							
							} // end of class AtomIterator_name
							_HOCL_atomIteratorArray7[0] = new AtomIterator_name();
						}
						MoleculeIterator[] _HOCL_moleculeIteratorArray7 = new MoleculeIterator[0];
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray7, _HOCL_moleculeIteratorArray7);
						return perm;
					}
				
				} // class IteratorSolution13
				_HOCL_atomIteratorArrayTuple6[1] = new IteratorSolution13();
			}
			_HOCL_atomIteratorArray4[3] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple6, Gw_call.this);
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple7;
			_HOCL_atomIteratorArrayTuple7 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal7 extends IteratorForExternal {
					public AtomIterator__HOCL_literal7(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator8 = (IteratorForTuple)permutation.getAtomIterator(4);
							String _HOCL_literal7 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator8.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal7.equals("ENV");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal7
				_HOCL_atomIteratorArrayTuple7[0] = new AtomIterator__HOCL_literal7();
			}
			{
				class IteratorSolution15 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray8;
						_HOCL_atomIteratorArray8 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray8 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray8[0] = new MoleculeIterator(1); // w_env
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray8, _HOCL_moleculeIteratorArray8);
						return perm;
					}
				
				} // class IteratorSolution15
				_HOCL_atomIteratorArrayTuple7[1] = new IteratorSolution15();
			}
			_HOCL_atomIteratorArray4[4] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple7, Gw_call.this);
		}
		MoleculeIterator[] _HOCL_moleculeIteratorArray9 = new MoleculeIterator[0];
		permutation = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray4, _HOCL_moleculeIteratorArray9);
	}
	public Gw_call clone() {
		 return new Gw_call();
	}

	public void addType(String s){}

	// compute result of the rule gw_call
	protected Molecule computeResult(){
		ExternalObject object;
		ReactionRule rule;
		String[] string;
		Tuple tuple;
		HoclList list;		
		IteratorForTuple _HOCL_tupleAtomIterator9 = (IteratorForTuple)permutation.getAtomIterator(3);
		IteratorForSolution _HOCL_iteratorSolution1 = (IteratorForSolution)_HOCL_tupleAtomIterator9.getAtomIterator(1);
		String name = (String)((IteratorForExternal)_HOCL_iteratorSolution1.getSubPermutation().getAtomIterator(0)).getObject();
		IteratorForTuple _HOCL_tupleAtomIterator10 = (IteratorForTuple)permutation.getAtomIterator(2);
		IteratorForSolution _HOCL_iteratorSolution2 = (IteratorForSolution)_HOCL_tupleAtomIterator10.getAtomIterator(1);
		Molecule w_par = _HOCL_iteratorSolution2.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator11 = (IteratorForTuple)permutation.getAtomIterator(4);
		IteratorForSolution _HOCL_iteratorSolution3 = (IteratorForSolution)_HOCL_tupleAtomIterator11.getAtomIterator(1);
		Molecule w_env = _HOCL_iteratorSolution3.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator12 = (IteratorForTuple)permutation.getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution4 = (IteratorForSolution)_HOCL_tupleAtomIterator12.getAtomIterator(1);
		String service = (String)((IteratorForExternal)_HOCL_iteratorSolution4.getSubPermutation().getAtomIterator(0)).getObject();
		// new Molecule  
		Molecule mol4 = new Molecule();
		Tuple tuple3 = new Tuple(2);
		tuple3.set(0, ExternalObject.getHOCL_TypeTranslation("SRC"));
		// new solution + 0 
		Solution solution3 = new Solution(); 
		{
			// new Molecule  
			Molecule mol5 = new Molecule();
			solution3.addMolecule(mol5);
		}
		tuple3.set(1, solution3);
		tuple = tuple3;
		mol4.add(tuple);
		this.addType("Tuple");
		
		
		Tuple tuple4 = new Tuple(2);
		tuple4.set(0, ExternalObject.getHOCL_TypeTranslation("SRV"));
		// new solution + 0 
		Solution solution4 = new Solution(); 
		{
			// new Molecule  
			Molecule mol6 = new Molecule();
			{ 
			Atom atomReference = ExternalObject.getHOCL_TypeTranslation(service);
			if (atomReference instanceof ExternalObject) {
				ExternalObject auxObject = (ExternalObject) atomReference;
				mol6.add(auxObject);
				string = auxObject.getObject().getClass().toString().split("\\.");
				this.addType(string[string.length-1]);
			} else {
				mol6.add(atomReference);
			if(atomReference instanceof Tuple) 
				this.addType("Tuple");
			}
			} 
			
			solution4.addMolecule(mol6);
		}
		tuple4.set(1, solution4);
		tuple = tuple4;
		mol4.add(tuple);
		this.addType("Tuple");
		
		
		Tuple tuple5 = new Tuple(2);
		tuple5.set(0, ExternalObject.getHOCL_TypeTranslation("RESULT"));
		// new solution + 0 
		Solution solution5 = new Solution(); 
		{
			// new Molecule  
			Molecule mol7 = new Molecule();
			Tuple tuple6 = new Tuple(2);
			tuple6.set(0, ExternalObject.getHOCL_TypeTranslation("TRANSFER"));
			tuple6.set(1, ExternalObject.getHOCL_TypeTranslation(Invoker.call(name, service, w_par, w_env)));
			tuple = tuple6;
			mol7.add(tuple);
			this.addType("Tuple");
			
			
			solution5.addMolecule(mol7);
		}
		tuple5.set(1, solution5);
		tuple = tuple5;
		mol4.add(tuple);
		this.addType("Tuple");
		
		
		Tuple tuple7 = new Tuple(2);
		tuple7.set(0, ExternalObject.getHOCL_TypeTranslation("NAME"));
		// new solution + 0 
		Solution solution6 = new Solution(); 
		{
			// new Molecule  
			Molecule mol8 = new Molecule();
			{ 
			Atom atomReference = ExternalObject.getHOCL_TypeTranslation(name);
			if (atomReference instanceof ExternalObject) {
				ExternalObject auxObject = (ExternalObject) atomReference;
				mol8.add(auxObject);
				string = auxObject.getObject().getClass().toString().split("\\.");
				this.addType(string[string.length-1]);
			} else {
				mol8.add(atomReference);
			if(atomReference instanceof Tuple) 
				this.addType("Tuple");
			}
			} 
			
			solution6.addMolecule(mol8);
		}
		tuple7.set(1, solution6);
		tuple = tuple7;
		mol4.add(tuple);
		this.addType("Tuple");
		
		
		Tuple tuple8 = new Tuple(2);
		tuple8.set(0, ExternalObject.getHOCL_TypeTranslation("ENV"));
		// new solution + 0 
		Solution solution7 = new Solution(); 
		{
			// new Molecule  
			Molecule mol9 = new Molecule();
			mol9.add(w_env);
			solution7.addMolecule(mol9);
		}
		tuple8.set(1, solution7);
		tuple = tuple8;
		mol4.add(tuple);
		this.addType("Tuple");
		
		
		return mol4;
	}

} // end of class Gw_call
