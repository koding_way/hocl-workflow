/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.rules.distributed;
import fr.inria.ginflow.invoker.Invoker;
import fr.inria.ginflow.distributed.TransferMolecule;
import fr.inria.hocl.core.hocli.*;
import java.io.*;

public class Gw_receive extends ReactionRule implements Serializable {

	
	public Gw_receive(){
		super("gw_receive", Shot.N_SHOT);
		setTrope(Trope.REDUCER);
			AtomIterator[] _HOCL_atomIteratorArray29;
		_HOCL_atomIteratorArray29 = new AtomIterator[3];
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple22;
			_HOCL_atomIteratorArrayTuple22 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal22 extends IteratorForExternal {
					public AtomIterator__HOCL_literal22(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator61 = (IteratorForTuple)permutation.getAtomIterator(0);
							String _HOCL_literal22 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator61.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal22.equals("IN");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal22
				_HOCL_atomIteratorArrayTuple22[0] = new AtomIterator__HOCL_literal22();
			}
			{
				class IteratorSolution45 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray29;
						_HOCL_atomIteratorArray29 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray29 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray29[0] = new MoleculeIterator(1); // w_in
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray29, _HOCL_moleculeIteratorArray29);
						return perm;
					}
				
				} // class IteratorSolution45
				_HOCL_atomIteratorArrayTuple22[1] = new IteratorSolution45();
			}
			_HOCL_atomIteratorArray29[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple22, Gw_receive.this);
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple23;
			_HOCL_atomIteratorArrayTuple23 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal23 extends IteratorForExternal {
					public AtomIterator__HOCL_literal23(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator62 = (IteratorForTuple)permutation.getAtomIterator(1);
							String _HOCL_literal23 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator62.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal23.equals("SRC");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal23
				_HOCL_atomIteratorArrayTuple23[0] = new AtomIterator__HOCL_literal23();
			}
			{
				class IteratorSolution47 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray30;
						_HOCL_atomIteratorArray30 = new AtomIterator[1];
						{
							class AtomIterator_provenance extends IteratorForExternal {
								public AtomIterator_provenance(){
									access = Access.REWRITE;
								}
								@Override
								public boolean conditionSatisfied() {
									Atom atom;
									boolean satisfied;
									atom = iterator.getElement();
									satisfied = false;
									if (atom instanceof ExternalObject
									  && ((ExternalObject)atom).getObject() instanceof String) {
										satisfied = true;
									}
									return satisfied;
								}
							
							} // end of class AtomIterator_provenance
							_HOCL_atomIteratorArray30[0] = new AtomIterator_provenance();
						}
						MoleculeIterator[] _HOCL_moleculeIteratorArray30 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray30[0] = new MoleculeIterator(1); // w_src
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray30, _HOCL_moleculeIteratorArray30);
						return perm;
					}
				
				} // class IteratorSolution47
				_HOCL_atomIteratorArrayTuple23[1] = new IteratorSolution47();
			}
			_HOCL_atomIteratorArray29[1] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple23, Gw_receive.this);
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple24;
			_HOCL_atomIteratorArrayTuple24 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal24 extends IteratorForExternal {
					public AtomIterator__HOCL_literal24(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator63 = (IteratorForTuple)permutation.getAtomIterator(2);
							String _HOCL_literal24 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator63.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal24.equals("TRANSFER");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal24
				_HOCL_atomIteratorArrayTuple24[0] = new AtomIterator__HOCL_literal24();
			}
			{
				class IteratorSolution55 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray31;
						_HOCL_atomIteratorArray31 = new AtomIterator[3];
						{
							AtomIterator[] _HOCL_atomIteratorArrayTuple25;
							_HOCL_atomIteratorArrayTuple25 = new AtomIterator[2];
							{
								class AtomIterator__HOCL_literal25 extends IteratorForExternal {
									public AtomIterator__HOCL_literal25(){
										access = Access.REWRITE;
									}
									@Override
									public boolean conditionSatisfied() {
										Atom atom;
										boolean satisfied;
										atom = iterator.getElement();
										satisfied = false;
										if (atom instanceof ExternalObject
										  && ((ExternalObject)atom).getObject() instanceof String) {
											
											IteratorForTuple _HOCL_tupleAtomIterator64 = (IteratorForTuple)permutation.getAtomIterator(2);
											IteratorForSolution _HOCL_iteratorSolution39 = (IteratorForSolution)_HOCL_tupleAtomIterator64.getAtomIterator(1);
											IteratorForTuple _HOCL_tupleAtomIterator65 = (IteratorForTuple)_HOCL_iteratorSolution39.getSubPermutation().getAtomIterator(0);
											String _HOCL_literal25 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator65.getAtomIterator(0)).getObject();
											satisfied = _HOCL_literal25.equals("OUT");
										}
										return satisfied;
									}
								
								} // end of class AtomIterator__HOCL_literal25
								_HOCL_atomIteratorArrayTuple25[0] = new AtomIterator__HOCL_literal25();
							}
							{
								class IteratorSolution49 extends IteratorForSolution {
									protected Permutation makeSubPermutation(){
										Permutation perm;
										AtomIterator[] _HOCL_atomIteratorArray31;
										_HOCL_atomIteratorArray31 = new AtomIterator[0];
										
										MoleculeIterator[] _HOCL_moleculeIteratorArray31 = new MoleculeIterator[1];
										_HOCL_moleculeIteratorArray31[0] = new MoleculeIterator(1); // w_out
										
										perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray31, _HOCL_moleculeIteratorArray31);
										return perm;
									}
								
								} // class IteratorSolution49
								_HOCL_atomIteratorArrayTuple25[1] = new IteratorSolution49();
							}
							_HOCL_atomIteratorArray31[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple25, Gw_receive.this);
						}
						{
							AtomIterator[] _HOCL_atomIteratorArrayTuple26;
							_HOCL_atomIteratorArrayTuple26 = new AtomIterator[2];
							{
								class AtomIterator__HOCL_literal26 extends IteratorForExternal {
									public AtomIterator__HOCL_literal26(){
										access = Access.REWRITE;
									}
									@Override
									public boolean conditionSatisfied() {
										Atom atom;
										boolean satisfied;
										atom = iterator.getElement();
										satisfied = false;
										if (atom instanceof ExternalObject
										  && ((ExternalObject)atom).getObject() instanceof String) {
											
											IteratorForTuple _HOCL_tupleAtomIterator66 = (IteratorForTuple)permutation.getAtomIterator(2);
											IteratorForSolution _HOCL_iteratorSolution40 = (IteratorForSolution)_HOCL_tupleAtomIterator66.getAtomIterator(1);
											IteratorForTuple _HOCL_tupleAtomIterator67 = (IteratorForTuple)_HOCL_iteratorSolution40.getSubPermutation().getAtomIterator(1);
											String _HOCL_literal26 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator67.getAtomIterator(0)).getObject();
											satisfied = _HOCL_literal26.equals("NAME");
										}
										return satisfied;
									}
								
								} // end of class AtomIterator__HOCL_literal26
								_HOCL_atomIteratorArrayTuple26[0] = new AtomIterator__HOCL_literal26();
							}
							{
								class IteratorSolution51 extends IteratorForSolution {
									protected Permutation makeSubPermutation(){
										Permutation perm;
										AtomIterator[] _HOCL_atomIteratorArray32;
										_HOCL_atomIteratorArray32 = new AtomIterator[1];
										{
											class AtomIterator_s extends IteratorForExternal {
												public AtomIterator_s(){
													access = Access.REWRITE;
												}
												@Override
												public boolean conditionSatisfied() {
													Atom atom;
													boolean satisfied;
													atom = iterator.getElement();
													satisfied = false;
													if (atom instanceof ExternalObject
													  && ((ExternalObject)atom).getObject() instanceof String) {
														satisfied = true;
													}
													return satisfied;
												}
											
											} // end of class AtomIterator_s
											_HOCL_atomIteratorArray32[0] = new AtomIterator_s();
										}
										MoleculeIterator[] _HOCL_moleculeIteratorArray32 = new MoleculeIterator[0];
										perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray32, _HOCL_moleculeIteratorArray32);
										return perm;
									}
								
								} // class IteratorSolution51
								_HOCL_atomIteratorArrayTuple26[1] = new IteratorSolution51();
							}
							_HOCL_atomIteratorArray31[1] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple26, Gw_receive.this);
						}
						{
							AtomIterator[] _HOCL_atomIteratorArrayTuple27;
							_HOCL_atomIteratorArrayTuple27 = new AtomIterator[2];
							{
								class AtomIterator__HOCL_literal27 extends IteratorForExternal {
									public AtomIterator__HOCL_literal27(){
										access = Access.REWRITE;
									}
									@Override
									public boolean conditionSatisfied() {
										Atom atom;
										boolean satisfied;
										atom = iterator.getElement();
										satisfied = false;
										if (atom instanceof ExternalObject
										  && ((ExternalObject)atom).getObject() instanceof String) {
											
											IteratorForTuple _HOCL_tupleAtomIterator68 = (IteratorForTuple)permutation.getAtomIterator(2);
											IteratorForSolution _HOCL_iteratorSolution41 = (IteratorForSolution)_HOCL_tupleAtomIterator68.getAtomIterator(1);
											IteratorForTuple _HOCL_tupleAtomIterator69 = (IteratorForTuple)_HOCL_iteratorSolution41.getSubPermutation().getAtomIterator(2);
											String _HOCL_literal27 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator69.getAtomIterator(0)).getObject();
											satisfied = _HOCL_literal27.equals("EXIT");
										}
										return satisfied;
									}
								
								} // end of class AtomIterator__HOCL_literal27
								_HOCL_atomIteratorArrayTuple27[0] = new AtomIterator__HOCL_literal27();
							}
							{
								class IteratorSolution53 extends IteratorForSolution {
									protected Permutation makeSubPermutation(){
										Permutation perm;
										AtomIterator[] _HOCL_atomIteratorArray33;
										_HOCL_atomIteratorArray33 = new AtomIterator[1];
										{
											class AtomIterator_e extends IteratorForExternal {
												public AtomIterator_e(){
													access = Access.REWRITE;
												}
												@Override
												public boolean conditionSatisfied() {
													Atom atom;
													boolean satisfied;
													atom = iterator.getElement();
													satisfied = false;
													if (atom instanceof ExternalObject
													  && ((ExternalObject)atom).getObject() instanceof Integer) {
														
														IteratorForTuple _HOCL_tupleAtomIterator70 = (IteratorForTuple)permutation.getAtomIterator(2);
														IteratorForSolution _HOCL_iteratorSolution42 = (IteratorForSolution)_HOCL_tupleAtomIterator70.getAtomIterator(1);
														IteratorForTuple _HOCL_tupleAtomIterator71 = (IteratorForTuple)_HOCL_iteratorSolution42.getSubPermutation().getAtomIterator(1);
														IteratorForSolution _HOCL_iteratorSolution43 = (IteratorForSolution)_HOCL_tupleAtomIterator71.getAtomIterator(1);
														String s = (String)((IteratorForExternal)_HOCL_iteratorSolution43.getSubPermutation().getAtomIterator(0)).getObject();
														IteratorForTuple _HOCL_tupleAtomIterator72 = (IteratorForTuple)permutation.getAtomIterator(1);
														IteratorForSolution _HOCL_iteratorSolution44 = (IteratorForSolution)_HOCL_tupleAtomIterator72.getAtomIterator(1);
														String provenance = (String)((IteratorForExternal)_HOCL_iteratorSolution44.getSubPermutation().getAtomIterator(0)).getObject();
														satisfied = (s.equals(provenance));
													}
													return satisfied;
												}
											
											} // end of class AtomIterator_e
											_HOCL_atomIteratorArray33[0] = new AtomIterator_e();
										}
										MoleculeIterator[] _HOCL_moleculeIteratorArray33 = new MoleculeIterator[0];
										perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray33, _HOCL_moleculeIteratorArray33);
										return perm;
									}
								
								} // class IteratorSolution53
								_HOCL_atomIteratorArrayTuple27[1] = new IteratorSolution53();
							}
							_HOCL_atomIteratorArray31[2] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple27, Gw_receive.this);
						}
						MoleculeIterator[] _HOCL_moleculeIteratorArray34 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray34[0] = new MoleculeIterator(1); // w_transfer
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray31, _HOCL_moleculeIteratorArray34);
						return perm;
					}
				
				} // class IteratorSolution55
				_HOCL_atomIteratorArrayTuple24[1] = new IteratorSolution55();
			}
			_HOCL_atomIteratorArray29[2] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple24, Gw_receive.this);
		}
		MoleculeIterator[] _HOCL_moleculeIteratorArray35 = new MoleculeIterator[0];
		permutation = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray29, _HOCL_moleculeIteratorArray35);
	}
	public Gw_receive clone() {
		 return new Gw_receive();
	}

	public void addType(String s){}

	// compute result of the rule gw_receive
	protected Molecule computeResult(){
		ExternalObject object;
		ReactionRule rule;
		String[] string;
		Tuple tuple;
		HoclList list;		
		IteratorForTuple _HOCL_tupleAtomIterator73 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution45 = (IteratorForSolution)_HOCL_tupleAtomIterator73.getAtomIterator(1);
		Molecule w_in = _HOCL_iteratorSolution45.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator74 = (IteratorForTuple)permutation.getAtomIterator(2);
		IteratorForSolution _HOCL_iteratorSolution46 = (IteratorForSolution)_HOCL_tupleAtomIterator74.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator75 = (IteratorForTuple)_HOCL_iteratorSolution46.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution47 = (IteratorForSolution)_HOCL_tupleAtomIterator75.getAtomIterator(1);
		Molecule w_out = _HOCL_iteratorSolution47.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator76 = (IteratorForTuple)permutation.getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution48 = (IteratorForSolution)_HOCL_tupleAtomIterator76.getAtomIterator(1);
		Molecule w_src = _HOCL_iteratorSolution48.getSubPermutation().getMatchedMolecule(0);
		// new Molecule  
		Molecule mol34 = new Molecule();
		Tuple tuple31 = new Tuple(2);
		tuple31.set(0, ExternalObject.getHOCL_TypeTranslation("SRC"));
		// new solution + 0 
		Solution solution27 = new Solution(); 
		{
			// new Molecule  
			Molecule mol35 = new Molecule();
			mol35.add(w_src);
			solution27.addMolecule(mol35);
		}
		tuple31.set(1, solution27);
		tuple = tuple31;
		mol34.add(tuple);
		this.addType("Tuple");
		
		
		Tuple tuple32 = new Tuple(2);
		tuple32.set(0, ExternalObject.getHOCL_TypeTranslation("IN"));
		// new solution + 0 
		Solution solution28 = new Solution(); 
		{
			// new Molecule  
			Molecule mol36 = new Molecule();
			mol36.add(w_out);
			mol36.add(w_in);
			solution28.addMolecule(mol36);
		}
		tuple32.set(1, solution28);
		tuple = tuple32;
		mol34.add(tuple);
		this.addType("Tuple");
		
		
		return mol34;
	}

} // end of class Gw_receive
