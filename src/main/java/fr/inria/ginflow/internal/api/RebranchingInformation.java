/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.internal.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 
 * Keep all what is needed to perform a rebranching.
 * 
 * @author msimonin
 *
 */
public class RebranchingInformation implements Serializable {
    
    /** Serial id.*/
	private static final long serialVersionUID = 1L;

	/** Keep track of the rebranching. (src of the alternatives).*/
    private Map<String, Set<String>> updateSrc_; 
    
    /** Keep track of the rebranching. (src control of the alternatives).*/
    private Map<String, Set<String>> updateSrcControl_; 
    
    /** Keep track of the rebranching. (dst of the alternatives).*/
    private Map<String, Set<String>> updateDst_;
    
    /** Keep track of the rebranching. (dst of the alternatives).*/
    private Map<String, Set<String>> updateDstControl_;
    
    /** List of the supervised services (they'll send "adapt" to each keys of 
     * updateSrc/Dst)*/
    private List<String> supervised_;

    public static RebranchingInformation newRebranchingInformation(){
    	return new RebranchingInformation();
    }
    
    public RebranchingInformation() {
        updateSrc_ = new HashMap<String, Set<String>>();
        updateSrcControl_ = new HashMap<String, Set<String>>();
        updateDst_ = new HashMap<String, Set<String>>();
        updateDstControl_ = new HashMap<String, Set<String>>();
        supervised_ = new ArrayList<String>(); 
    }
   
    public RebranchingInformation addSupervised(String name) {
        supervised_.add(name);
        return this;
    }
    
    public RebranchingInformation addAllSupervised(Collection<String> names) {
        supervised_.addAll(names);
        return this;
    }
    
    public RebranchingInformation addUpdateSrc(String src, String newDst) {
        add(updateSrc_, src, newDst);
        return this;
    }
    
    public RebranchingInformation addAllUpdateSrc(String src, Collection<String> newDsts) {
        addAll(updateSrc_, src, newDsts);
        return this;
    }
    
    public RebranchingInformation addUpdateSrcControl(String src, String newDst) {
        add(updateSrcControl_, src, newDst);
        return this;
    }
    
    public RebranchingInformation addAllUpdateSrcControl(String src, Collection<String> newDsts) {
        addAll(updateSrcControl_, src, newDsts);
        return this;
    }

    
    public RebranchingInformation addUpdateDst(String dst, String newSrc) {
        add(updateDst_, dst, newSrc);
        return this;
    }
    
    public RebranchingInformation addAllUpdateDst(String dst, Collection<String> newSrcs) {
        addAll(updateDst_, dst, newSrcs);
        return this;
    }
    
    public RebranchingInformation addUpdateDstControl(String dst, String newSrc) {
        add(updateDstControl_, dst, newSrc);
        return this;
    }
    
    public RebranchingInformation addAllUpdateDstControl(String dst, Collection<String> newSrcs) {
        addAll(updateDstControl_, dst, newSrcs);
        return this;
    }
    
    /**
     * @return the supervised
     */
    public List<String> getSupervised() {
        return supervised_;
    }

    /**
     * @param supervised the supervised to set
     */
    public void setSupervised(List<String> supervised) {
        supervised_ = supervised;
    }

    /**
     * @return the updateSrc
     */
    public Map<String, Set<String>> getUpdateSrc() {
        return updateSrc_;
    }

    /**
     * @param updateSrc the updateSrc to set
     */
    public void setUpdateSrc(Map<String, Set<String>> updateSrc) {
        updateSrc_ = updateSrc;
    }

    /**
     * @return the updateSrcControl
     */
    public Map<String, Set<String>> getUpdateSrcControl() {
        return updateSrcControl_;
    }

    /**
     * @param updateSrcControl the updateSrcControl to set
     */
    public void setUpdateSrcControl(Map<String, Set<String>> updateSrcControl) {
        updateSrcControl_ = updateSrcControl;
    }

    /**
     * @return the updateDst
     */
    public Map<String, Set<String>> getUpdateDst() {
        return updateDst_;
    }

    /**
     * @param updateDst the updateDst to set
     */
    public void setUpdateDst(Map<String, Set<String>> updateDst) {
        updateDst_ = updateDst;
    }

    /**
     * @return the updateDstControl
     */
    public Map<String, Set<String>> getUpdateDstControl() {
        return updateDstControl_;
    }

    /**
     * @param updateDstControl the updateDstControl to set
     */
    public void setUpdateDstControl(Map<String, Set<String>> updateDstControl) {
        updateDstControl_ = updateDstControl;
    }

    private void add(Map<String, Set<String>> updateHashSet, String src, String newDst) {
        if (updateHashSet.get(src) == null) {
            updateHashSet.put(src, new LinkedHashSet<String>());
        }
        updateHashSet.get(src).add(newDst);
    }
    
    private void addAll(Map<String, Set<String>> updateHashSet, String src, Collection<String> newDsts) {
        if (updateHashSet.get(src) == null) {
            updateHashSet.put(src, new LinkedHashSet<String>());
        }
        updateHashSet.get(src).addAll(newDsts);
    }
}
