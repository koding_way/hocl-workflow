/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.internal.api;

import fr.inria.hocl.core.hocli.ExternalObject;

/**
 * 
 * Source "IN":&lt;in1, ..., inn&gt;
 * 
 * @author msimonin
 * 
 */
public class In extends GinflowTuple {

	/** Default serial id. */
	private static final long serialVersionUID = 1L;

	/**
	 * Build a new in.
	 * 
	 * @return The in under construction
	 * 
	 */
	public static In newIn() {
		return new In();
	}

	private In() {
		super(String.valueOf(GinflowType.IN));
	}

	/**
	 * 
	 * Check wheter the destinations contains the value.
	 * 
	 * @param value
	 *            The value to check
	 * @return true iff the value is found
	 */
	public boolean contains(String value) {
		return contains(new ExternalObject(value));
	}

	/**
	 * 
	 * Check wheter the destinations contains the value.
	 * 
	 * @param value
	 *            The value to check
	 * @return true iff the value is found
	 */
	public boolean contains(double value) {
		return contains(new ExternalObject(value));
	}

	public In add(Object in) {
		// check the available types:
		if (in instanceof Double) {
			value_.addAtom(new ExternalObject((Double) in));
		} else if (in instanceof String) {
			value_.addAtom(new ExternalObject((String) in));
		}
		return this;

	}

}
