/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.external.json;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ginflow.exceptions.GinflowException;
import fr.inria.ginflow.internal.api.GinflowTuple;
import fr.inria.hocl.core.hocli.Atom;
import fr.inria.hocl.core.hocli.ExternalObject;
import fr.inria.hocl.core.hocli.SimpleIterator;
import fr.inria.hocl.core.hocli.Tuple;

/**
 * 
 * 
 * @author msimonin
 * 
 */
public class ServiceSerializer extends JsonSerializer<Tuple> {

    /** The logger. */
    final static Logger log_ = LoggerFactory.getLogger(ServiceSerializer.class);

    @Override
    public void serialize(Tuple tuple, JsonGenerator jg,
            SerializerProvider provider) throws IOException,
            JsonProcessingException {
        boolean isGinflowTuple = GinflowTuple.isGinflowTuple(tuple);
        if (!isGinflowTuple) {
            throw new IOException(GinflowException.INVALID_GINFLOW_TUPLE);
        }
        GinflowTuple gTuple = new GinflowTuple(tuple);
        jg.writeStartObject();

        // we iterate over the values.
        SimpleIterator<Atom> it = gTuple.getValue().newIterator();
        Atom atom;
        while ((atom = it.next()) != null) {
            if (GinflowTuple.isGinflowTuple(atom)) {
                // we serialize recursively.
                serializeInternalGTuple((Tuple) atom, jg, provider);
            }
        }
        jg.writeEndObject();

    }

    private void serializeInternalGTuple(Tuple tuple, JsonGenerator jg,
            SerializerProvider provider) throws IOException {
        boolean isGinflowTuple = GinflowTuple.isGinflowTuple(tuple);
        if (!isGinflowTuple) {
            throw new IOException(GinflowException.INVALID_GINFLOW_TUPLE);
        }
        GinflowTuple gTuple = new GinflowTuple(tuple);

        jg.writeArrayFieldStart(((String) gTuple.getKey().getObject())
                .toLowerCase());
        SimpleIterator<Atom> it = gTuple.getValue().newIterator();
        Atom atom;

        while ((atom = it.next()) != null) {
            if (GinflowTuple.isGinflowTuple(atom)) {
                // we serialize recursively.
                jg.writeStartObject();
                serializeInternalGTuple((Tuple) atom, jg, provider);
                jg.writeEndObject();
            } else if (!(atom instanceof ExternalObject)) {
                throw new IOException(GinflowException.INVALID_GINFLOW_TUPLE);
            } else {
                // we override the toString method of external object due to
                // extra quotes
                // that could be added in case of a String.
                Object object = ((ExternalObject) atom).getObject();
                if (object instanceof String) {
                    jg.writeString((String) object);
                } else {
                    jg.writeString(object.toString());
                }
            }
        }
        jg.writeEndArray();
    }

}
