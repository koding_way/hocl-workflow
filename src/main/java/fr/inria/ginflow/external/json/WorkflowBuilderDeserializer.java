/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.external.json;

import java.io.IOException;

import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.deser.std.StdDeserializer;
import org.codehaus.jackson.type.JavaType;

import fr.inria.ginflow.external.api.ServiceBuilder;
import fr.inria.ginflow.external.api.WorkflowBuilder;
import fr.inria.ginflow.internal.api.RebranchingInformation;

/**
 * 
 * Workflow deserializer.
 * 
 * @author msimonin
 * 
 */
public class WorkflowBuilderDeserializer extends
        StdDeserializer<WorkflowBuilder> {

    /** workflow name field. */
    private static final String FIELD_NAME = "name";

    /** List of services field. */
    private static final String FIELD_SERVICES = "services";
    
    /** List of rebranchings*/
    private static final String FIELD_REBRANCHINGS = "rebranchings";
    

    protected WorkflowBuilderDeserializer(Class<?> vc) {
        super(vc);
    }

    public WorkflowBuilderDeserializer(JavaType valueType) {
        super(valueType);
    }

    @Override
    public WorkflowBuilder deserialize(JsonParser jp,
            DeserializationContext context) throws IOException,
            JsonProcessingException {
        WorkflowBuilder wBuilder = WorkflowBuilder.newWorkflowBuilder();
        ServiceBuilderDeserializer sBuilderDeserializer = new ServiceBuilderDeserializer(
                ServiceBuilder.class);

        if (jp.getCurrentToken() != JsonToken.START_OBJECT) {
            throw new IOException("invalid start marker");
        }

        while (jp.nextToken() != JsonToken.END_OBJECT) {
            String fieldname = jp.getCurrentName();
            jp.nextToken();

            if (FIELD_NAME.equals(fieldname)) {
                wBuilder.setName(jp.getText());
            } else if (FIELD_SERVICES.equals(fieldname)) {
                if (jp.getCurrentToken() != JsonToken.START_ARRAY) {
                    throw new IOException("invalid array marker");
                }
                while (jp.nextToken() != JsonToken.END_ARRAY) {
                    ServiceBuilder sBuilder = sBuilderDeserializer.deserialize(
                            jp, context);
                    wBuilder.addServiceBuilder(sBuilder);
                }
            } else if (FIELD_REBRANCHINGS.equals(fieldname)) {
            	if (jp.getCurrentToken() != JsonToken.START_ARRAY) {
                    throw new IOException("invalid array marker");
                }
            	ObjectMapper mapper = new ObjectMapper();
            	while (jp.nextToken() != JsonToken.END_ARRAY) {
            		RebranchingInformation ri = mapper.readValue(jp, RebranchingInformation.class);
            		wBuilder.addRebranchingInformation(ri);
            	}
            		
            }
        }
        jp.close();
        return wBuilder;
    }
}
