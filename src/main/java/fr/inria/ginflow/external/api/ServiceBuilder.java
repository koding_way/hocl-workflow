/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.external.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import fr.inria.ginflow.exceptions.GinflowException;
import fr.inria.ginflow.internal.api.GinflowTuple;
import fr.inria.ginflow.internal.api.Service;

/**
 * 
 * Build a new service.
 * 
 * @author msimonin
 *
 */
public class ServiceBuilder {

    /** Name of the service.*/
    private String name_;
    
    /** Ginflow tuples representation (will be added to the solution)*/
    private HashMap<String, List<Object>> ginflowTuples_;

	private int alternativeGroup_;

	private int supervisedGroup_;

    public static ServiceBuilder newBuilder() {
        return new ServiceBuilder();
       
    }
    
    private  ServiceBuilder() {
        super();
        ginflowTuples_ = new HashMap<String, List<Object>>();
        alternativeGroup_ = -1;
        supervisedGroup_ = -1;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name_;
    }


    /**
     * 
     * @param name the name to set
     * 
     * @return The service builder under construction.
     */
    public ServiceBuilder setName(String name) {
        name_ = name;
        return this;
    }

    /**
     * @return the ginflowTuples
     */
    public HashMap<String, List<Object>> getGinflowTuples() {
        return ginflowTuples_;
    }

   
    /**
     * 
     * 
     * @param ginflowTuples the ginflowTuples to set
     * 
     * @return	The Service builder under construction.
     */
    public ServiceBuilder setGinflowTuples(HashMap<String, List<Object>> ginflowTuples) {
        ginflowTuples_ = ginflowTuples;
        return this;
    }
    
    /**
     * 
     * Add a value to a ginflow Tuple.
     * 
     * @param key       The key
     * @param value     The value
     * @return  The current service builder 
     */
    public ServiceBuilder add(String key, Object value) {
        List<Object> ginflowTuple = ginflowTuples_.get(key);
        if (ginflowTuple == null){
            ginflowTuples_.put(key, new ArrayList<Object>(Arrays.asList(value)));
        } else {
            ginflowTuple.add(value);
        }
        return this;
    }
    
    /**
     * 
     * Add all values to a ginflow Tuple.
     * 
     * @param key       The key
     * @param values    The values
     * @return The current service builder
     */
    public ServiceBuilder addAll(String key, List<Object> values) {
        List<Object> ginflowTuple = ginflowTuples_.get(key);
        if (ginflowTuple == null){
            ginflowTuples_.put(key, values);
        } else {
            ginflowTuple.addAll(values);
        }
        return this;
    }
    

    /**
     * 
     * Helper method to get the list of values whose key is *key*
     * 
     * @param key   The key to retrieve the values from.
     * @return  The list of values.  
     */
    public List<Object> get(String key) {
        return ginflowTuples_.get(key);
    }
    
    /**
     * 
     * Build (and validates) the Ginflow representation of the service described
     * by this builder. 
     * 
     * @return  The Ginflow representation of the service.
     * 
     * @throws GinflowException	GinflowException
     */
    public Service build() throws GinflowException {
        if (name_ == null) {
            throw new GinflowException(GinflowException.MUST_GIVE_A_NAME);
        }
        Service service = Service.newService(name_);
        try {
            for (Entry<String, List<Object>> entry : ginflowTuples_.entrySet()) {
                String key = entry.getKey();
                List<Object> values = entry.getValue();
                GinflowTuple gTuple = new GinflowTuple(key, values);
                service.setGinflowTuple(gTuple);
            }
        } catch (Exception e) {            
            throw new GinflowException(e);
        }
        return service;
    }

    /**
     * @return the alternatives
     */
    public boolean isAlternative() {
        return alternativeGroup_ != -1;
    }

   
    /**
     * @return the supervised
     */
    public boolean isSupervised() {
        return supervisedGroup_ != -1;
    }

   
	public String getAlternativeGroup() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getSupervisedGroup() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setAlternativeGroup(int group) {
		alternativeGroup_ = group;
	}

	public void setSupervisedGroup(int group) {
		supervisedGroup_ = group;
		
	}
}
