/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.cli;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import ch.qos.logback.core.joran.spi.JoranException;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.MissingCommandException;
import com.beust.jcommander.ParameterException;

import fr.inria.ginflow.cli.command.CleanCommand;
import fr.inria.ginflow.cli.command.CommandMain;
import fr.inria.ginflow.cli.command.CommonCommand;
import fr.inria.ginflow.cli.command.LaunchCommand;
import fr.inria.ginflow.cli.command.RestartCommand;
import fr.inria.ginflow.cli.command.UpdateCommand;
import fr.inria.ginflow.cli.handler.CleanCommandHandler;
import fr.inria.ginflow.cli.handler.LaunchCommandHandler;
import fr.inria.ginflow.cli.handler.RestartCommandHandler;
import fr.inria.ginflow.cli.handler.UpdateCommandHandler;
import fr.inria.ginflow.exceptions.GinFlowExecutorException;
import fr.inria.ginflow.exceptions.GinflowException;
import fr.inria.ginflow.options.OptionException;

/**
 * 
 * Entry point of the command line interface.
 * 
 * @author msimonin
 *
 */
public class Main {



    public static void main(String[] args) {
        
        CommandMain cm = new CommandMain();
        JCommander jcommander = new JCommander(cm);
        LaunchCommand launchCommand = new LaunchCommand();
        jcommander.addCommand(launchCommand.getName(), launchCommand);
        CleanCommand cleanCommand = new CleanCommand();
        jcommander.addCommand(cleanCommand.getName(), cleanCommand);
        UpdateCommand updateCommand = new UpdateCommand();
        jcommander.addCommand(updateCommand.getName(), updateCommand);
        RestartCommand restartCommand = new RestartCommand();
        jcommander.addCommand(restartCommand.getName(), restartCommand);
        
        try{
            // Parse inputs args
            jcommander.parse(args);
            if (jcommander.getParsedCommand() == null) {
                throw new MissingCommandException("Missing command");
            }
            if (jcommander.getParsedCommand().equals(LaunchCommand.name)) {
                checkHelp(jcommander, launchCommand);
                LaunchCommandHandler launchCommandHandler = new LaunchCommandHandler(launchCommand);
                launchCommandHandler.dispatchCommand();
            } else if (jcommander.getParsedCommand().equals(CleanCommand.name)) {
                checkHelp(jcommander, cleanCommand);
                CleanCommandHandler cleanCommandHandler = new CleanCommandHandler(cleanCommand);
                cleanCommandHandler.dispatchCommand();
            } else if (jcommander.getParsedCommand().equals(UpdateCommand.name)) {
                checkHelp(jcommander, updateCommand);
                UpdateCommandHandler updateCommandHandler = new UpdateCommandHandler(updateCommand);
                updateCommandHandler.dispatchCommand();
            } else if (jcommander.getParsedCommand().equals(RestartCommand.name)) {
                checkHelp(jcommander, updateCommand);
                RestartCommandHandler restartCommandHandler = new RestartCommandHandler(restartCommand);
                restartCommandHandler.dispatchCommand();
            } else {    
                throw new MissingCommandException("Wrong command");
            }
        } catch(MissingCommandException e) {
            System.out.println(e.getMessage());
            displayAvailableCommand(jcommander);
        } catch(ParameterException parameterException) {
            System.err.println("Wrong parameter description : "  + parameterException.getMessage());
            System.out.println("-h or --help will give you the usage");
            System.exit(1);
        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JsonMappingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (GinflowException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (OptionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JoranException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (GinFlowExecutorException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private static void displayAvailableCommand(JCommander jcommander) {
        System.out.println("Available commands are : ");
        for (String command : jcommander.getCommands().keySet()) {
            System.out.println("\t" + command);
        }
        
    }

    private static void checkHelp(JCommander jcommander, CommonCommand command) {
        if (command.isHelp()) {
            jcommander.usage(command.getName());
            System.exit(0);
        }
    }
}
