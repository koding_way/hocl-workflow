/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.cli.command;

import com.beust.jcommander.Parameter;


/**
 * 
 * Description of the command line interface.
 * 
 * @author msimonin
 *
 */
public class UpdateCommand extends CommonCommand {

    public static final String name = "update";

    @Parameter(names = {"-w", "--workflow"},
            description = "Path to the json file containing the update to apply to the workflow",
            required = true
            )
    private String workflowFile_;
    
    @Parameter(names = {"-id", "--uid"},
            description = "workflow id",
            required = true
            )
    private String workflowId_;

	public UpdateCommand() {
        super();
    }


    /**
     * @return the workflowFile
     */
    public String getWorkflowFile() {
        return workflowFile_;
    }

    /**
     * @param workflowFile the workflowFile to set
     */
    public void setWorkflowFile(String workflowFile) {
        workflowFile_ = workflowFile;
    }

    @Override
    public String getName() {
        return name;
    }

    /**
     * @return the workflowId
     */
    public String getWorkflowId() {
        return workflowId_;
    }
    
    

    /**
     * @param workflowId the workflowId to set
     */
    public void setWorkflowId(String workflowId) {
        workflowId_ = workflowId;
    }

        
}