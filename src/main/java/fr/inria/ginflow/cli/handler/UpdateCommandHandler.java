/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.cli.handler;

import java.io.File;
import java.io.IOException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ch.qos.logback.core.joran.spi.JoranException;
import fr.inria.ginflow.cli.command.UpdateCommand;
import fr.inria.ginflow.distributed.communication.CommunicationFactory;
import fr.inria.ginflow.distributed.communication.CommunicationType;
import fr.inria.ginflow.distributed.communication.api.SimplePublisher;
import fr.inria.ginflow.exceptions.GinFlowExecutorException;
import fr.inria.ginflow.exceptions.GinflowException;
import fr.inria.ginflow.external.api.WorkflowBuilder;
import fr.inria.ginflow.external.json.GinflowModule;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.options.OptionException;
import fr.inria.ginflow.options.Options;

/**
 * 
 * Command handler : update the workflow on the fly.
 * 
 * @author msimonin
 * 
 */
public class UpdateCommandHandler extends CommonCommandHandler {

    /** The logger. */
    final static Logger log = LoggerFactory
            .getLogger(UpdateCommandHandler.class);

    /** Parsed command. */
    private UpdateCommand ginflowCommand_;

    public UpdateCommandHandler(UpdateCommand ginflowCommand) {
        super(ginflowCommand);
        ginflowCommand_ = ginflowCommand;
    }

    /**
     * Execute the command - Read the json file - create the workflow - create
     * the relevant executor - launch the workflow - retrieve results
     * 
     * @throws IOException				IOException
     * @throws JsonMappingException		JsonMappingException
     * @throws JsonParseException		JsonParseException
     * @throws GinflowException			GinflowException
     * @throws OptionException			OptionException
     * @throws JoranException			JoranException
     * @throws GinFlowExecutorException	GinFlowExecutorException
     */
    public void dispatchCommand() throws JsonParseException,
            JsonMappingException, IOException, GinflowException,
            OptionException, JoranException, GinFlowExecutorException {

        configureLogger();

        // - read the json file
        String workflowFile = ginflowCommand_.getWorkflowFile();
        String workflowId = ginflowCommand_.getWorkflowId();
        
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new GinflowModule());
        WorkflowBuilder wBuilder = mapper.readValue(new File(workflowFile),
                WorkflowBuilder.class);
        
        log.info("Initializing the workflow");
        // create the workflow
        Workflow workflow = wBuilder.build();
        log.info("Updating workflow {}", workflowId);
        Options options = loadOptions();
        //this code is broker specific

        try {
            // publish on workflow specific queue (default to multiset)
            String destinationString = workflowId;
            SimplePublisher publisher = CommunicationFactory.newSimplePublisher(options);
            publisher.publish(workflow, CommunicationType.updateQueue(workflowId));
            publisher.close();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }    

}
