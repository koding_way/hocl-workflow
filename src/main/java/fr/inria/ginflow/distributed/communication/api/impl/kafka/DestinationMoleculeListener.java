/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.communication.api.impl.kafka;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kafka.consumer.Consumer;
import kafka.consumer.ConsumerConfig;
import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;
import kafka.message.MessageAndMetadata;
import kafka.serializer.StringDecoder;
import fr.inria.ginflow.distributed.DestinationMolecule;
import fr.inria.ginflow.listener.ServiceListener;
import fr.inria.ginflow.options.Options;

/**
 * 
 * Listens to workflow topic, received (potentially) reduced description.
 *  
 * @author msimonin
 *
 */
public class DestinationMoleculeListener extends Thread {


    /** The logger. */
    final static Logger log_ = LoggerFactory
            .getLogger(DestinationMoleculeListener.class);
    
	
    /** The workflow Id.*/
    private String workflowId_;
    
    /** The options.*/
    private Map<String, String> options_;
    
    /** The listener (callback when moleucle arrives).*/
    private ServiceListener listener_;

    /** The Kafka consumer.*/
    private ConsumerConnector consumer;

    /** Topic to listen from.*/
    private String topic_;

    /**
     * 
     * Constructor. 
     * 
     * @param workflowId    the workflowId.
     * @param options       the options.
     * @param listener      the central multiset listener.
     */
    public DestinationMoleculeListener(String workflowId,
            Map<String, String> options, ServiceListener listener) {
       workflowId_ = workflowId;
       options_ = options;
       listener_ = listener;
       topic_ = workflowId_;
       consumer = Consumer.createJavaConsumerConnector(createConsumerConfig());
       log_.debug("Starting the Destination Molecule Listener");
    }

    /**
     * 
     * Creates the prop config required by Kafka.
     * 
     * @return  The consumer config.
     */
    private ConsumerConfig createConsumerConfig() {
        Properties props = new Properties();
        props.put("zookeeper.connect", options_.get(Options.KAFKA_ZOOKEEPER_CONNECT));
        props.put("group.id", UUID.randomUUID().toString());
        props.put("zk.sessiontimeout.ms", "400");
        props.put("zk.synctime.ms", "200");
        props.put("autocommit.interval.ms", "1000");
        // we start consuming from the "beginning"
        props.put("auto.offset.reset", "smallest");

        return new ConsumerConfig(props);
    }

    @Override
    public void run() {
    	log_.debug("Start consuming message from " + topic_);
        Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
        topicCountMap.put(topic_, new Integer(1));
       
        Map<String, List<KafkaStream<String, Object>>> consumerMap = consumer.createMessageStreams(
                topicCountMap,
                new StringDecoder(null),
                new ObjectDecoder());
        
        KafkaStream<String, Object> streams = consumerMap.get(topic_).get(0);
        
        ConsumerIterator<String, Object> it = streams.iterator();
        while (it.hasNext()) {
            MessageAndMetadata<String, Object> m = it.next();
            listener_.onServiceUpdated((DestinationMolecule) m.message());
        }
    }

    /**
     * Closes the listener.
     */
    public void close() {
       consumer.shutdown();
        
    }    

}
