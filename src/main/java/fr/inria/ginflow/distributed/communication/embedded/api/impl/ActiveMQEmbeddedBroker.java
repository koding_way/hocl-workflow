/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.communication.embedded.api.impl;

import java.util.Map;

import org.apache.activemq.broker.BrokerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ginflow.distributed.communication.embedded.api.EmbeddedBroker;
import fr.inria.ginflow.exceptions.GinflowException;
import fr.inria.ginflow.internal.api.Service;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.listener.external.api.ExternalWorkflowListener;
import fr.inria.ginflow.options.Options;

public class ActiveMQEmbeddedBroker implements EmbeddedBroker {

	/** Logger. */
	private static final Logger log_ = LoggerFactory.getLogger(ActiveMQEmbeddedBroker.class);

	/** The options. */
	private Map<String, String> options_;

	private BrokerService broker_;

	private String url_;

	/**
	 * 
	 * Constructor.
	 * 
	 * @param options
	 *            The options
	 */
	public ActiveMQEmbeddedBroker(Map<String, String> options) {
		options_ = options;
		broker_ = new BrokerService();
		url_ = options_.get(Options.ACTIVEMQ_BROKER_URL);
		log_.info("Starting the broker on {}", url_);
		broker_.setPersistent(false);
		broker_.setStartAsync(true);
		broker_.setUseJmx(false);
	}

	/**
	 * Starting the broker
	 * 
	 * @throws GinflowException
	 *             GinflowException
	 */
	@Override
	public void start() throws GinflowException {
		log_.debug("Starting a new embedded broker");

		try {
			int attempt = 0;
			int limit = 5;
			broker_.addConnector(url_);
			broker_.start();
			while (!broker_.isStarted() && attempt < limit) {
				log_.debug("Waiting for the broker to be started");
				Thread.sleep(1000);
			}
			if (attempt >= limit) {
				throw new GinflowException("Unable to start the broker, max attempt reached");
			}
		} catch (Exception e) {
			log_.error(e.getMessage());
			throw new GinflowException(e.getMessage());
		}

	}

	/**
	 * Stopping the broker.
	 */
	@Override
	public void stop() {
		if (!broker_.isStarted()) {
			return;
		}
		try {
			broker_.stop();
		} catch (Exception e) {
			log_.error(e.getMessage());
		}
	}

}
