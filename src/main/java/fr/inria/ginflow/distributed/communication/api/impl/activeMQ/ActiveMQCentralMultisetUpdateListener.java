/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.communication.api.impl.activeMQ;

import java.util.Map;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ginflow.distributed.DestinationMolecule;
import fr.inria.ginflow.distributed.communication.CommunicationType;
import fr.inria.ginflow.distributed.communication.api.CentralMultisetListener;
import fr.inria.ginflow.distributed.communication.api.CentralMultisetUpdateListener;
import fr.inria.ginflow.exceptions.CommunicationException;
import fr.inria.ginflow.exceptions.GinFlowExecutorException;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.listener.ServiceListener;
import fr.inria.ginflow.listener.WorkflowListener;
import fr.inria.ginflow.options.Options;

public class ActiveMQCentralMultisetUpdateListener extends CentralMultisetUpdateListener implements MessageListener {
	/** The logger. */
    final static Logger log_ = LoggerFactory
            .getLogger(ActiveMQCentralMultisetUpdateListener.class);

    /** url of the borker. */
    private String url_;

    /* connection to the broker. */
    private Connection connection_;

    public ActiveMQCentralMultisetUpdateListener(String workflowId,
            Map<String, String> options, WorkflowListener listener) {
        super(workflowId, options, listener);
        String destinationString = CommunicationType.updateQueue(workflowId);
        log_.debug("Starting the ActiveMQCentralMultisetUpdateListener on " + destinationString);
        url_ = options_.get(Options.ACTIVEMQ_BROKER_URL);
        try {
            ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(
                    url_);
            connection_ = factory.createConnection();
            connection_.start();
            Session session = connection_.createSession(false,
                    Session.AUTO_ACKNOWLEDGE);
            Destination queue = session.createQueue(destinationString);

            MessageConsumer consumer = session.createConsumer(queue);
            consumer.setMessageListener(this);
        } catch (Exception e) {
            log_.error("Error while connecting to ActiveMQ", e);
        }
        log_.debug("ActiveMQCentralMultisetUpdateListener initialized");

    }

    @Override
    public void listen() {
        log_.debug("Waiting for messages...");
    }

    /**
     * 
     * Routine called when a molecule is received.
     * 
     */
    @Override
    public void onMessage(Message message) {
    	log_.debug("Received a message");
        try {
            if (message instanceof TextMessage) {
                TextMessage text = (TextMessage) message;
                log_.debug("Received a text Message "
                        + text.getText());
            } else {
                ObjectMessage obj = (ObjectMessage) message;
                log_.debug("Received a workflow update");
                Workflow update = (Workflow) obj.getObject();
                // TODO put this in a queue so that it is consumed sequentially
                listener_.onWorkflowUpdated(update);
            }
        } catch (JMSException e) {
            log_.error("Error during onMessage", e);
        } catch (GinFlowExecutorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Override
    public void close() throws CommunicationException {
        log_.debug("Closing the central multiset listener");
        try {
            connection_.close();
        } catch (JMSException e) {
            throw new CommunicationException(e);
        }
    }

}
