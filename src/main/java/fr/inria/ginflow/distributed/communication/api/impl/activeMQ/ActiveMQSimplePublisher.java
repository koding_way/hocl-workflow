/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.communication.api.impl.activeMQ;

import java.io.Serializable;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ginflow.distributed.communication.api.SimplePublisher;
import fr.inria.ginflow.exceptions.CommunicationException;
import fr.inria.ginflow.options.Options;

public class ActiveMQSimplePublisher extends SimplePublisher {

    /** The logger. */
    final static Logger log_ = LoggerFactory
            .getLogger(ActiveMQSimplePublisher.class);

    private Connection connection_;

    public ActiveMQSimplePublisher(Map<String, String> options) {
        super(options);
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(
                options_.get(Options.ACTIVEMQ_BROKER_URL));
        try {
            connection_ = factory.createConnection();
            connection_.start();
        } catch (JMSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        log_.debug("ActiveMQServiceInvokerPublisher initialized");
    }

    @Override
    public void publish(Serializable molecule, String destination)
            throws Exception {
        log_.debug("Publishing a molecule");
        Session session = connection_.createSession(false,
                Session.AUTO_ACKNOWLEDGE);
        
        //Topic topic = session.createTopic(topicString);
        log_.debug("Transfering a molecule {} to {}", molecule, destination);
        Destination queue = session.createQueue(destination);
        MessageProducer publisher = session.createProducer(queue);
        publisher.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        ObjectMessage msg = session.createObjectMessage();
        msg.setObject(molecule);
        publisher.send(msg);
        session.close();
        log_.debug("Published a molecule");

    }

    @Override
    public void close() throws CommunicationException {
        log_.debug("Closing the publisher");
        try {
            connection_.close();
        } catch (JMSException e) {
            throw new CommunicationException(e);
        }
    }

}
