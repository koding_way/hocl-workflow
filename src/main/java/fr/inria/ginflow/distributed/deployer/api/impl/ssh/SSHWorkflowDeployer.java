/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.deployer.api.impl.ssh;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ginflow.distributed.deployer.api.Command;
import fr.inria.ginflow.distributed.deployer.api.WorkflowDeployer;
import fr.inria.ginflow.exceptions.GinFlowExecutorException;
import fr.inria.ginflow.executor.api.impl.dist.ssh.listener.DeploymentListener;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.listener.WorkflowListener;
import fr.inria.ginflow.options.Options;


/**
 * 
 * @author msimonin
 *
 */
public class SSHWorkflowDeployer implements WorkflowDeployer, DeploymentListener{

    /** Logger. */
    private static final Logger log_ = LoggerFactory.getLogger(SSHWorkflowDeployer.class);
    /** List of machines to run services. */
    private ArrayList<String> machines_;
    
    /** The workflow id.*/
	private String workflowId_;
	
	/** The ginflow command to launch on nodes.*/
	private String ginflowCommandExecutor_;
	
	/** The options.*/
	private Map<String, String> options_;
	private WorkflowListener workflowListener_;
    
	public SSHWorkflowDeployer(Workflow workflow, Map<String, String> options) {
		options_ = options;
	    machines_ = Options.extractMachines(options.get(Options.SSH_MACHINES));
	
	    // initialize centralMultiset
	    workflowId_ = workflow.getId();
	  
	    ginflowCommandExecutor_ = String
	            .format("java -cp %s/%s %s",
	                    options.get(Options.DISTRIBUTION_HOME),
	                    options.get(Options.DISTRIBUTION_NAME),
	                    LocalExecutor.class.getName()
	            		);
	}
	
	@Override
	public void deploy(Workflow workflow) throws GinFlowExecutorException {
		 List<Command> commands = new ArrayList<Command>();
	        for (String sName : workflow.getServicesNames()) {
	        	String cmd = String
	                    .format("nohup %s -w '%s' -n '%s' %s > /tmp/%s.log 0</dev/null 2>&1 &",
	                            ginflowCommandExecutor_, workflowId_, sName,
	                            Options.toCommandLine(options_), sName);
	            Command command = new Command(sName, cmd);
	            commands.add(command);
	        }
	        // build the machine list using Round Robin policy
	        List<String> machines = new ArrayList<String>();
	        int i = 0;
	        for (@SuppressWarnings("unused")
	        Command command : commands) {
	            machines.add(machines_.get(i % machines_.size()));
	            i++;
	        }

	        try (RemoteExecutor remoteExecutor = new GanySshRemoteExecutor(
	                options_.get(Options.SSH_USERNAME), 
	                new File(options_.get(Options.SSH_PRIVATE_KEY)),
	                options_.get(Options.SSH_PASSWORD),
	                this
	        		)) {

	            // parallel execution
	            remoteExecutor.execParallel(machines, commands);

	        } catch (Exception e) {
	            log_.error(e.getMessage());
	            throw new GinFlowExecutorException(e);
	        }
		
	}

	@Override
	public void notifyDeploymentSuccess(Command command) {
		workflowListener_.onServiceDeployed(command.getId());
		
	}


    /**
     * Clean all the service invoker on all machines.
     */
	@Override
    public void clean() {
        log_.info("Cleaning remaining processes");
        String grepCommand = String.format("LocalExecutor -w %s", workflowId_);
        String killCommand = String.format("ps -ef|" + "grep '%s'|"
                + "grep -v grep|" + "awk '{print $2}'|" + "xargs kill",
                grepCommand);
        try (RemoteExecutor remoteExecutor = new GanySshRemoteExecutor(
                options_.get(Options.SSH_USERNAME), new File(
                        options_.get(Options.SSH_PRIVATE_KEY)),
                options_.get(Options.SSH_PASSWORD),
        		null
        		)) {
            remoteExecutor.execParallel(machines_, new Command("", killCommand));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

	@Override
	public void setWorkflowListener(WorkflowListener workflowListener) {
		workflowListener_ = workflowListener;
	}


}
