/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.deployer.api.impl.mesos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.mesos.Protos.CommandInfo;
import org.apache.mesos.Protos.CommandInfo.URI;
import org.apache.mesos.Protos.ExecutorID;
import org.apache.mesos.Protos.ExecutorInfo;
import org.apache.mesos.Protos.FrameworkID;
import org.apache.mesos.Protos.MasterInfo;
import org.apache.mesos.Protos.Offer;
import org.apache.mesos.Protos.OfferID;
import org.apache.mesos.Protos.Resource;
import org.apache.mesos.Protos.SlaveID;
import org.apache.mesos.Protos.TaskID;
import org.apache.mesos.Protos.TaskInfo;
import org.apache.mesos.Protos.TaskStatus;
import org.apache.mesos.Protos.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.mesos.Protos;
import org.apache.mesos.Scheduler;
import org.apache.mesos.SchedulerDriver;

import fr.inria.ginflow.distributed.deployer.api.Command;
import fr.inria.ginflow.executor.api.impl.dist.ssh.listener.DeploymentListener;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.options.Options;

public class MesosGinflowScheduler implements Scheduler {

	/** The logger. */
	final static Logger log_ = LoggerFactory.getLogger(MesosGinflowScheduler.class);

	/** service to launch indexed by wid.*/
	Map<String, Queue<String>> workflowsToLaunch_;
	/** services thaht has been successfully launched indexed by wid.*/
	private Map<String, Queue<String>> launchedWorkflowServices;
	/** original list of services indexed by wid.*/
	Map<String, Queue<String>> originalWorkflows_;

	// list of already deployed workflows.
	List<String> deployedWorkflows_ ;
	
	/** Mapping between taskid and service name. */
	private Map<TaskID, String> taskMapping_;

	private Workflow workflow_;

	private String workflowId_;

	private String ginflowCommandExecutor_;

	private Map<String, String> options_;

	private URI uri_;

	private DeploymentListener listener_;

	

	public MesosGinflowScheduler(Workflow workflow, Map<String, String> options, DeploymentListener listener) {
		workflow_ = workflow;
		workflowId_ = workflow.getId();
		options_ = options;
		listener_ = listener;
		
		workflowsToLaunch_ = new HashMap<String, Queue<String>>();
		launchedWorkflowServices= new HashMap<String, Queue<String>>();
		originalWorkflows_ = new HashMap<String, Queue<String>>();
		deployedWorkflows_ = new ArrayList<String>();
		taskMapping_ = new HashMap<TaskID, String>();
		
		String path_ = options_.get(Options.DISTRIBUTION_HOME) + "/" + options_.get(Options.DISTRIBUTION_NAME);
		uri_ = CommandInfo.URI.newBuilder().setValue(path_).setExtract(false).build();
		
		ginflowCommandExecutor_ = String.format(
				"java -cp %s %s ",
				options_.get(Options.DISTRIBUTION_NAME),
				LocalExecutor.class.getName()
				);

	}

	public String deploy(Workflow workflow) {
		
		String wid = UUID.randomUUID().toString();
		// initialize a new list of services to launch
		List<String> sNames = workflow.getServicesNames();
		log_.debug("deploying {} services with did = {}", sNames.size(), wid);
		workflowsToLaunch_.put(wid, new LinkedBlockingQueue<String>(sNames));
		// save the original mapping
		originalWorkflows_.put(wid, new LinkedBlockingQueue<String>(sNames));
		launchedWorkflowServices.put(wid, new LinkedBlockingQueue<String>());
		return wid;
	}

	public boolean isDeployed(String deploymentId) {
		return deployedWorkflows_.contains(deploymentId);
	}

	@Override
	public void registered(SchedulerDriver driver, FrameworkID frameworkId, MasterInfo masterInfo) {
		// TODO Auto-gen¬erated method stub

	}

	@Override
	public void reregistered(SchedulerDriver driver, MasterInfo masterInfo) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void resourceOffers(SchedulerDriver schedulerDriver, List<Offer> offers) {
		log_.debug("Offer received : " + offers);
		synchronized (this) {

			// we start a new service if needed
			// different policies could be implemented here.
			for (Protos.Offer offer : offers) {
				// one offer can be used for launching several tasks.
				// String sName = servicesToBeLaunched_.poll();
				String sName = pickAService();
				log_.debug("picked {}", sName);
				if (sName == null) {
					// no more service to launch for now.
					schedulerDriver.declineOffer(offer.getId());
				} else {
					List<Protos.TaskInfo> tasksToLaunch = new ArrayList<Protos.TaskInfo>();
					Collection<OfferID> of = new ArrayList<Protos.OfferID>();
					Protos.TaskID taskId = Protos.TaskID.newBuilder().setValue(sName).build();
					// update servicesToBeLaunched
					// servicesToBeLaunched_.remove(sName);
					taskMapping_.put(taskId, sName);

					// create the executor
					String executorCmd = String.format("%s -w %s -n %s %s", ginflowCommandExecutor_, workflowId_, sName,
							Options.toCommandLine(options_));
					log_.debug("Launching the executor {}", executorCmd);
					CommandInfo ginflowExecutorCommand = CommandInfo.newBuilder().setValue(executorCmd).addUris(uri_)
							.setUser(options_.get(Options.MESOS_USER)).build();

					ExecutorInfo ginflowExecutor = ExecutorInfo.newBuilder()
							.setExecutorId(ExecutorID.newBuilder().setValue("GinFlowExecutor " + sName))
							.setCommand(ginflowExecutorCommand).setName("Service Executor (Java)").setSource("java")
							.build();

					// Retrieve the wanted cpu and mem to use.
					double cpu = Double.valueOf(options_.get(Options.MESOS_RESOURCES_CPU));
					double mem = Double.valueOf(options_.get(Options.MESOS_RESOURCES_MEM));

					// generate a unique task ID
					TaskInfo task = TaskInfo.newBuilder().setName("task " + taskId.getValue()).setTaskId(taskId)
							.setSlaveId(offer.getSlaveId())
							.addResources(Resource.newBuilder().setName("cpus").setType(Value.Type.SCALAR)
									.setScalar(Value.Scalar.newBuilder().setValue(cpu)))
							.addResources(Resource.newBuilder().setName("mem").setType(Value.Type.SCALAR)
									.setScalar(Value.Scalar.newBuilder().setValue(mem)))
							// .setData(ByteString.copyFromUtf8(crawlQueue.get(0)))
							.setExecutor(ExecutorInfo.newBuilder(ginflowExecutor)).build();
					log_.info("GinFlowExecutor for service {} is scheduled on {}", sName, offer.getHostname());

					tasksToLaunch.add(task);
					of.add(offer.getId());
					Protos.Filters filters = Protos.Filters.newBuilder().setRefuseSeconds(1).build();
					schedulerDriver.launchTasks(of, tasksToLaunch, filters);
				}
			}
		}

	}

	/**
	 * 
	 * Check if there is at least one service to deploy
	 * and return it.
	 * The policy here is very naive
	 * 
	 * @return the next service to launch
	 */
	private String pickAService() {
		for (Queue<String> queue : workflowsToLaunch_.values()) {
			if (queue.size() > 0) {
				return queue.poll();
			}
		}
		return null;
	}
	
	@Override
	public void offerRescinded(SchedulerDriver driver, OfferID offerId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void statusUpdate(SchedulerDriver driver, TaskStatus status) {
		log_.debug("statusUpdate", status);
        TaskID taskid = status.getTaskId();
        // the task id it came from
        String sName = taskMapping_.get(taskid);
        // the wid it came from
        String wid = getWidFromService(sName);
        // taskid == sName
        switch (status.getState()) {
        case TASK_LOST:
        case TASK_ERROR:
        case TASK_FAILED:
            log_.debug("Task error with task id {} for service {}",
                    taskid.getValue(), sName);
            // reschedule it in the same queue.
            workflowsToLaunch_.get(wid).add(sName);
            taskMapping_.remove(taskid);
            break;
        case TASK_FINISHED:
            log_.debug("Task finished task id {} for service {}",
                    taskid.getValue(), sName);
            break;
        case TASK_KILLED:
            log_.debug("Task killed task id {} for service {}",
                    taskid.getValue(), sName);
            break;
        case TASK_RUNNING:
            log_.debug("Task running task id {} for service {}",
                    taskid.getValue(), sName);
            Queue<String> launchedServices = launchedWorkflowServices.get(wid); 
            launchedServices.add(sName);
            listener_.notifyDeploymentSuccess(new Command(sName, ""));
            if (launchedServices.size() == originalWorkflows_.get(wid).size()) {
            	deployedWorkflows_.add(wid);
            }
            break;
        case TASK_STAGING:
            break;
        case TASK_STARTING:
            break;
        default:
            break;
        }

	}

	private String getWidFromService(String sName) {
		for (Entry<String, Queue<String>> entry : originalWorkflows_.entrySet()) {
			if (entry.getValue().contains(sName)) {
				return entry.getKey();
			}
		}
		// it shouldn't happen
		return null;
	}

	@Override
	public void frameworkMessage(SchedulerDriver driver, ExecutorID executorId, SlaveID slaveId, byte[] data) {
		// TODO Auto-generated method stub

	}

	@Override
	public void disconnected(SchedulerDriver driver) {
		// TODO Auto-generated method stub

	}

	@Override
	public void slaveLost(SchedulerDriver driver, SlaveID slaveId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void executorLost(SchedulerDriver driver, ExecutorID executorId, SlaveID slaveId, int status) {
		// TODO Auto-generated method stub

	}

	@Override
	public void error(SchedulerDriver driver, String message) {
		// TODO Auto-generated method stub

	}

}
