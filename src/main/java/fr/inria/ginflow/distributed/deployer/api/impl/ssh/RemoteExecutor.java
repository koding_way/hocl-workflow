/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.deployer.api.impl.ssh;

import java.util.List;

import fr.inria.ginflow.distributed.deployer.api.Command;

/**
 * 
 * Execute Remote Command
 * 
 * @author msimonin
 *
 */
public abstract class RemoteExecutor implements AutoCloseable 
{

    /**
     * 
     * Executes the command on the host
     * 
     * @param host          The host list
     * @param command      	The command list
     * @throws Exception 	Exception
     */
    public abstract void exec(String host, Command command) throws Exception;
    
    /**
     * 
     * Executes commands in parallel on hosts.
     * 
     * @param hosts         The host list
     * @param commands      The command list
     * @throws Exception 	Exception
     */
    public abstract void execParallel(List<String> hosts, List<Command> commands) throws Exception;
   
    /**
     * 
     * Executes command in parallel on hosts.
     * 
     * @param hosts         The host list
     * @param commands       The commands
     * @throws Exception 	Exception
     */
    public abstract void execParallel(List<String> hosts, Command commands) throws Exception;
    
    /**
     * 
     * Closes the exececutor.
     * 
     */
    public abstract void close();


    /**
     * 
     * TODO, see if we really need it
     * Sets the connection time out
     * 
     * @param pollingTimeout	polling timeout
     */
    public abstract void setPollingTimeout(int pollingTimeout);
    
    
}
