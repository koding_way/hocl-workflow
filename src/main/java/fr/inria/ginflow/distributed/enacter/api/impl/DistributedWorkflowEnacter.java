/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.enacter.api.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ginflow.distributed.enacter.api.WorkflowEnacter;
import fr.inria.ginflow.exceptions.GinFlowExecutorException;
import fr.inria.ginflow.executor.api.impl.dist.CentralMultiset;
import fr.inria.ginflow.internal.api.GinflowTuple;
import fr.inria.ginflow.internal.api.Service;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.listener.WorkflowListener;
import fr.inria.ginflow.options.Options;

/**
 * 
 * Enacts the workflow. Here it's about starting the multiset (Hector's way)
 * which will in turn publish the description of all the services. Maybe in the
 * future we won't be bound to HOCL...
 * 
 * @author msimonin
 *
 */
public class DistributedWorkflowEnacter implements WorkflowEnacter, WorkflowListener {

	/** The logger. */
	final static Logger log_ = LoggerFactory.getLogger(DistributedWorkflowEnacter.class);

	/** The options. */
	private Map<String, String> options_;

	/** The workflow. */
	private Workflow workflow_;

	/** The workflow id. */
	private String workflowId_;

	private CentralMultiset centralMultiset_;

	private Object lockObject_;

	/** a more global listener. */
	private WorkflowListener workflowListener_;

	public DistributedWorkflowEnacter(Workflow workflow, Map<String, String> options) {
		options_ = options;
		workflow_ = workflow;
		workflowId_ = workflow.getId();

		centralMultiset_ = new CentralMultiset(workflow_, workflowId_, options_, this);
		lockObject_ = new Object();
	}

	public void setWorkflowListener(WorkflowListener workflowListener) {
		workflowListener_ = workflowListener;
	}

	@Override
	public void enact(Workflow workflow) throws GinFlowExecutorException {
		centralMultiset_.start();
		waitCompletion();
	}

	@Override
	public void update(Workflow workflowUpdate) {
		centralMultiset_.update(workflowUpdate);
	}

	@Override
	public void restart() throws GinFlowExecutorException {
		log_.info("Restarting the multiset");
		centralMultiset_.restart();
		waitCompletion();
	}

	private void waitCompletion() throws GinFlowExecutorException {
		try {
			// we wait until the workflow is terminated
			synchronized (lockObject_) {
				lockObject_.wait();
			}
		} catch (NumberFormatException | InterruptedException e) {
			log_.error(e.getMessage());
			throw new GinFlowExecutorException(e);
		}
	}

	@Override
	public void shutdown() {
		// TODO?
	}

	@Override
	public void onWorkflowTerminated() {
		// publish upstream the result
		// release central multiset
		log_.debug("Workflow Terminated");
		synchronized (lockObject_) {
			lockObject_.notify();
		}
		if (workflowListener_ != null) {
			workflowListener_.onWorkflowTerminated();
		}

	}

	@Override
	public void onResultReceived(Service service) {
		// publish upstream the result
		if (workflowListener_ != null) {
			workflowListener_.onResultReceived(service);
		}

	}

	@Override
	public void serviceDescriptionSent(GinflowTuple service) {
		// publish upstream the result
		if (workflowListener_ != null) {
			workflowListener_.serviceDescriptionSent(service);
		}
	}

	@Override
	public void onWorkflowUpdated(Workflow update) throws GinFlowExecutorException {
		// not implemented here

	}

	@Override
	public void onServiceDeployed(String id) {
		// not implemented here

	}

}
