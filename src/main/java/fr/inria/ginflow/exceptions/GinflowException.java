/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.exceptions;

/**
 * 
 * Ginflow exception.
 * 
 * @author msimonin
 *
 */
public class GinflowException extends Exception {

    /** Default Serial id.*/
    private static final long serialVersionUID = 1L;
    public static final String MUST_GIVE_A_NAME = "You must give a name";
    
    /** Used in serialization.*/
    public static final String INVALID_GINFLOW_TUPLE = "The tuple is not a ginflow Tuple";

    public GinflowException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public GinflowException(String arg0, Throwable arg1) {
        super(arg0, arg1);
        // TODO Auto-generated constructor stub
    }

    public GinflowException(String arg0) {
        super(arg0);
        // TODO Auto-generated constructor stub
    }

    public GinflowException(Throwable arg0) {
        super(arg0);
        // TODO Auto-generated constructor stub
    }

}
