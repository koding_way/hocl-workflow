/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.listener;

import fr.inria.ginflow.exceptions.GinFlowExecutorException;
import fr.inria.ginflow.internal.api.GinflowTuple;
import fr.inria.ginflow.internal.api.Service;
import fr.inria.ginflow.internal.api.Workflow;

/**
 * 
 * Workflow Listener.
 * 
 * @author msimonin
 * 
 */
public interface WorkflowListener {

    /**
     * Called when the workflow is terminated.
     */
    void onWorkflowTerminated();

    
	/**
	 * Called when the result of a service is received
	 * 
	 * @param service	the service descriptio,
	 */
	void onResultReceived(Service service);

	/**
	 * Called when a service description has been sent
	 * 
	 * @param service	The service content.
	 */
	void serviceDescriptionSent(GinflowTuple service);

	/**
	 * 
	 * Called when the workflow is updated (adaptiveness)
	 * 
	 * @param update	Update the workflow
	 * @throws GinFlowExecutorException GinFlowExecutorException 
	 */
	void onWorkflowUpdated(Workflow update) throws GinFlowExecutorException;

	/**
	 * 
	 * Called when a service invoker is deployed.
	 * 
	 * @param id	The id.
	 */
	void onServiceDeployed(String id);

}
