/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.listener.external.api.impl;

import java.io.IOException;

import javax.websocket.OnClose;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.codehaus.jackson.map.ObjectMapper;


@ServerEndpoint(value = "/websocket")
public class WebSocketServer {
	@OnOpen
	public void onOpen(Session session) {
		System.out.println("Connected ... " + session.getId());
		WebListener.sessions.add(session);
		ObjectMapper mapper = new ObjectMapper();
		try {
			synchronized(WebListener.messages) {
				for (Message message : WebListener.messages) {
					session.getBasicRemote().sendText(mapper.writeValueAsString(message));
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@OnClose
	public void onClose(Session session) {
		WebListener.sessions.remove(session);
	}
}
