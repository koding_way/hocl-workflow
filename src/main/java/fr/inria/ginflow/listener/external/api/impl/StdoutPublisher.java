/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.listener.external.api.impl;

import java.io.File;
import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;

import fr.inria.ginflow.external.api.WorkflowBuilder;
import fr.inria.ginflow.external.json.GinflowModule;
import fr.inria.ginflow.internal.api.GinflowTuple;
import fr.inria.ginflow.internal.api.Service;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.listener.external.api.ExternalWorkflowListener;

public class StdoutPublisher implements ExternalWorkflowListener {

	
	@Override
	public void notifyWorkflowTerminated() {
		System.out.println("Workflow terminated");
		
	}

	@Override
	public void notifyResultReceived(Service service) {
		ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new GinflowModule());
        try {
        	String json = mapper.writeValueAsString(service);
        	System.out.println(String.format("[Service %s] Received result", service.getName()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void notifyWorkflowStarted(String workflow) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyDeployed(String id) {
		System.out.println(String.format("[Service %s] deployed", id));
	}

	@Override
	public void notifyServiceDescriptionSend(GinflowTuple service) {
		System.out.println(String.format("[Service %s] description sent", service));
		
	}

	@Override
	public void notifyWorkflowUpdate(Workflow updateWorkflow) {
		System.out.println("Workflow has been updated");
		
	}

}
