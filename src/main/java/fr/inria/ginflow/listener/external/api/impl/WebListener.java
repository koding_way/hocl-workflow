/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.listener.external.api.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.websocket.DeploymentException;
import javax.websocket.Session;

import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.tyrus.server.Server;

import fr.inria.ginflow.external.json.GinflowModule;
import fr.inria.ginflow.internal.api.GinflowTuple;
import fr.inria.ginflow.internal.api.Service;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.listener.external.api.ExternalWorkflowListener;

public class WebListener implements ExternalWorkflowListener {

	public static final List<Session> sessions = Collections.synchronizedList(new ArrayList<Session>()); 

	public static final List<Message> messages = Collections.synchronizedList(new ArrayList<Message>());


	
	/**toJson mapper.*/
	private ObjectMapper mapper_ ;

	private ObjectMapper defaultMapper_;
	
	public WebListener() {
		mapper_ = new ObjectMapper();
        mapper_.registerModule(new GinflowModule());
		
        defaultMapper_ = new ObjectMapper();
        
		Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(Server.STATIC_CONTENT_ROOT, "./");
		Server server = new Server("localhost", 8025, "/", properties, WebSocketServer.class);
		
		try {
			server.start();
		} catch (DeploymentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void notifyWorkflowTerminated() {
		// TODO Auto-generated method stub

	}

	@Override
	public void notifyResultReceived(Service service) {
		sendService(Message.RESULT_MESSAGE, service);
	}

	private void sendService(String type, GinflowTuple service) {
		try{
			String msg = mapper_.writeValueAsString(service);
			Message message = new Message(type, msg);
			for (Session session : sessions) {
				session.getAsyncRemote().sendText(defaultMapper_.writeValueAsString(message));
			}
			messages.add(message);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void notifyWorkflowStarted(String msg) {
		try{
			Message message = new Message(Message.WORKFLOW_MESSAGE, msg);
			for (Session session : sessions) {
				session.getAsyncRemote().sendText(defaultMapper_.writeValueAsString(message));
			}
			messages.add(message);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void notifyDeployed(String id) {
		try{
			Message message = new Message(Message.DEPLOYMENT_SUCCES_MESSAGE, id);
			for (Session session : sessions) {
				session.getAsyncRemote().sendText(defaultMapper_.writeValueAsString(message));
			}
			messages.add(message);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void notifyServiceDescriptionSend(GinflowTuple service) {
		sendService(Message.DESCRIPTION_SENT, service);
	}

	@Override
	public void notifyWorkflowUpdate(Workflow updateWorkflow) {
		// TODO Auto-generated method stub
		
	}
}
