/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.listener.external.api;

import fr.inria.ginflow.internal.api.GinflowTuple;
import fr.inria.ginflow.internal.api.Service;
import fr.inria.ginflow.internal.api.Workflow;

public interface ExternalWorkflowListener {


	/**
	 * 
	 * Notify that a workflow has started
	 * TODO pass the workflow here.
	 * 
	 * @param message	the message
	 */
	void notifyWorkflowStarted(String message);
	
	/***
	 * 
	 * Notify that the workflow is terminated.
	 * 
	 */
	void notifyWorkflowTerminated();

	
	/**
	 * Notifies that a result has been received
	 * @param service	The service
	 */
	void notifyResultReceived(Service service);

	
	/**
	 * 
	 * Notifies that a service is deployed
	 * 
	 * @param id	the id (name) of the service
	 */
	void notifyDeployed(String id);

	
	/**
	 * 
	 * Notifies that a service description has been sent
	 * 
	 * @param service	the serviceName of the service
	 */
	void notifyServiceDescriptionSend(GinflowTuple service);

	/**
	 * 
	 * Notifies thaht a workflow has been updated.
	 * 
	 * @param updateWorkflow	The workflow to update
	 */
	void notifyWorkflowUpdate(Workflow updateWorkflow);



}
